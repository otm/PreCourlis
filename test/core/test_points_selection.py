import unittest

from qgis.core import QgsFeatureRequest, QgsVectorLayer, QgsProject
from qgis.PyQt.QtCore import QItemSelectionModel

from PreCourlis.core.points_selection import PointsSelection
from PreCourlis.core.precourlis_file import PreCourlisFileLine
from PreCourlis.widgets.points_table_model import PointsTableModel

from .. import PROFILE_LINES_PATH


class TestPointsSelection(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestPointsSelection, cls).setUpClass()
        cls.layer = QgsVectorLayer(PROFILE_LINES_PATH, "profiles_lines", "ogr")
        assert cls.layer.isValid()

    @classmethod
    def tearDownClass(cls):
        cls.layer.deleteLater()
        super(TestPointsSelection, cls).tearDownClass()
        QgsProject.instance().clear()

    def section(self):
        request = QgsFeatureRequest()
        request.setLimit(1)
        feature = next(self.layer.getFeatures(request))
        return PreCourlisFileLine(self.layer).section_from_feature(feature)

    def test_init(self):
        PointsSelection()

    def test_open(self):
        obj = PointsSelection()
        obj.open()

        assert obj.memory_layer.isValid()
        assert obj.memory_layer in QgsProject.instance().mapLayers().values()

    def test_close(self):
        obj = PointsSelection()
        obj.open()
        obj.close()

        assert obj.memory_layer not in QgsProject.instance().mapLayers().values()

    def test_refresh(self):
        section = self.section()

        model = PointsTableModel()
        model.set_section(section)

        selection_model = QItemSelectionModel()
        selection_model.setModel(model)

        obj = PointsSelection()
        obj.open()
        obj.set_profiles_layers(self.layer)
        obj.set_section(section)
        obj.set_selection_model(selection_model)

        selection = selection_model.model().index(1, 2)
        selection_model.select(selection, QItemSelectionModel.ClearAndSelect)

        assert obj.memory_layer.featureCount() == 1

        selection = selection_model.model().index(5, 2)
        selection_model.select(selection, QItemSelectionModel.Select)

        assert obj.memory_layer.featureCount() == 2

        selection_model.clear()

        assert obj.memory_layer.featureCount() == 0
