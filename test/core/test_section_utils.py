import unittest
from numpy import array, any

from qgis.core import QgsFeatureRequest, QgsVectorLayer

from PreCourlis.lib.mascaret.mascaret_file import Section
from PreCourlis.core.precourlis_file import PreCourlisFileLine

from .. import PROFILE_LINES_PATH

from PreCourlis.core.section_utils import (
    check_section,
    move_up_invalid_points_from_section,
    move_down_invalid_points_from_section,
)


class TestCheckSection(unittest.TestCase):

    SECTION = Section(1, 5, 10.0)

    @classmethod
    def setUpClass(cls) -> None:
        cls.layer = QgsVectorLayer(PROFILE_LINES_PATH, "profiles_lines", "ogr")
        assert cls.layer.isValid()

        return super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        cls.layer.deleteLater()
        super().tearDownClass()

    def test_first_section_is_consistent(self):
        request = QgsFeatureRequest()
        request.setLimit(1)
        feature = next(self.layer.getFeatures(request))

        section = PreCourlisFileLine(self.layer).section_from_feature(feature)

        result = check_section(section)
        expected = array(
            [
                array([True, True, True, True, True, True, True, True, True]),
                array([True, True, True, True, True, True, True, True, True]),
            ]
        )

        assert (result == expected).all()

    def test_whenSedimentLayersAreCrossing_thenSectionIsNotConsistent(self):
        request = QgsFeatureRequest()
        request.setLimit(1)
        feature = next(self.layer.getFeatures(request))

        section = PreCourlisFileLine(self.layer).section_from_feature(feature)
        section.z = array([2, 2, 2, 2, 2, 2, 2, 2, 2])
        layer1arr = array([1, 1, 1, 3, 1, 1, 1, 3, 3])
        layer2arr = array([4, 0, 0, 0, 0, 4, 0, 4, 4])
        section.nlayers = 2
        section.layers_elev = array(
            [
                layer1arr,
                layer2arr,
            ]
        )

        result = check_section(section)
        expected = array(
            [
                array([True, True, True, False, True, True, True, False, False]),
                array([False, True, True, False, True, False, True, False, False]),
                array([False, True, True, True, True, False, True, False, False]),
            ]
        )

        assert (result == expected).all()

    def test_givenSedimentLayersCrossingEachOther_whenMoveUp_thenSectionIsConsistent(
        self,
    ):
        request = QgsFeatureRequest()
        request.setLimit(1)
        feature = next(self.layer.getFeatures(request))

        section = PreCourlisFileLine(self.layer).section_from_feature(feature)
        section.z = array([2, 2, 2, 2, 2, 2, 2, 2, 2])
        layer1arr = array([1, 1, 1, 3, 1, 1, 1, 3, 3])
        layer2arr = array([4, 0, 0, 0, 0, 4, 0, 4, 4])
        section.nlayers = 2
        section.layers_elev = array(
            [
                layer1arr,
                layer2arr,
            ]
        )

        move_up_invalid_points_from_section(section)

        expected = array(
            [
                array([4, 2, 2, 3, 2, 4, 2, 4, 4]),
                array([4, 1, 1, 3, 1, 4, 1, 4, 4]),
                array([4, 0, 0, 0, 0, 4, 0, 4, 4]),
            ]
        )

        assert (section.z == expected[0]).all()
        assert (section.layers_elev[0] == expected[1]).all()
        assert (section.layers_elev[1] == expected[2]).all()

        result = check_section(section)
        self.assertFalse(any(~result))

    def test_givenSedimentLayersCrossingEachOther_whenMoveDown_thenSectionIsConsistent(
        self,
    ):
        request = QgsFeatureRequest()
        request.setLimit(1)
        feature = next(self.layer.getFeatures(request))

        section = PreCourlisFileLine(self.layer).section_from_feature(feature)
        section.z = array([2, 2, 2, 2, 2, 2, 2, 2, 2])
        layer1arr = array([1, 1, 1, 3, 1, 1, 1, 3, 3])
        layer2arr = array([4, 0, 0, 0, 0, 4, 0, 4, 4])
        section.nlayers = 2
        section.layers_elev = array(
            [
                layer1arr,
                layer2arr,
            ]
        )

        move_down_invalid_points_from_section(section)

        expected = array(
            [
                array([2, 2, 2, 2, 2, 2, 2, 2, 2]),
                array([1, 1, 1, 2, 1, 1, 1, 2, 2]),
                array([1, 0, 0, 0, 0, 1, 0, 2, 2]),
            ]
        )

        assert (section.z == expected[0]).all()
        assert (section.layers_elev[0] == expected[1]).all()
        assert (section.layers_elev[1] == expected[2]).all()

        result = check_section(section)
        self.assertFalse(any(~result))
