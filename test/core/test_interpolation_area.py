import unittest
import json

from qgis.core import (
    QgsVectorLayer,
    QgsProject,
)

from PreCourlis.core.interpolation_area import (
    InterpolationArea,
    InterpolationAreaManager,
)
from .. import PROFILE_LINES_PATH


class TestInterpolationArea(unittest.TestCase):

    INTERPOLATION_AREA = InterpolationArea(1, 5, 10.0)

    INTERPOLATION_AREA_AS_DICT = {
        "from_profil_id": 1,
        "to_profil_id": 5,
        "longitudinal_step": 10.0,
    }

    def test_interpolationAreaAsDict_egual_interpolationAreaToDict(self):
        assert self.INTERPOLATION_AREA_AS_DICT == self.INTERPOLATION_AREA.toDict()

    def test_interpolationAreaFromDictConversion(self):
        assert self.INTERPOLATION_AREA == InterpolationArea.fromDict(
            self.INTERPOLATION_AREA_AS_DICT
        )

    def test_interpolationAreaToDictConversion(
        self,
    ):
        assert self.INTERPOLATION_AREA == InterpolationArea.fromDict(
            self.INTERPOLATION_AREA.toDict()
        )

    def test_interpolationAreaFromDictToDictConversion(
        self,
    ):
        assert (
            self.INTERPOLATION_AREA_AS_DICT
            == InterpolationArea.fromDict(self.INTERPOLATION_AREA_AS_DICT).toDict()
        )


class TestInterpolationAreaManager(unittest.TestCase):

    INTERPOLATION_AREAS = [
        InterpolationArea(1, 5, 100.0),
        InterpolationArea(5, 6, 40.0),
    ]

    INTERPOLATION_AREAS_STR = (
        "["
        '{"from_profil_id": 1,'
        '"to_profil_id": 5,'
        '"longitudinal_step": 100.0},'
        '{"from_profil_id": 5,'
        '"to_profil_id": 6,'
        '"longitudinal_step": 40.0}]'
    )

    def setUp(self):
        self.layer = QgsVectorLayer(PROFILE_LINES_PATH, "profiles_lines", "ogr")
        assert self.layer.isValid()
        QgsProject.instance().addMapLayer(self.layer)

    def tearDown(self) -> None:
        self.layer.deleteLater()
        QgsProject.instance().clear()

    def test_givenNoneLayer_whenReadingInterpolationAreasFromLayer_thenReturnEmptyArray(
        self,
    ):
        layer = None

        result = InterpolationAreaManager.readInterpolationAreasFromLayer(layer)

        assert result == []

    def test_givenLayer_whenReadingInterpolationAreasFromLayer_thenReturnEmptyArray(
        self,
    ):
        layer = QgsVectorLayer()

        result = InterpolationAreaManager.readInterpolationAreasFromLayer(layer)

        assert result == []

    def test_givenLayerWithProperties_whenReadingFromLayer_thenReturnCorrespondantArray(
        self,
    ):
        layer = QgsVectorLayer()
        layer.setCustomProperty(
            InterpolationAreaManager.INTERPOLATION_AREAS_KEY,
            json.dumps([ia.toDict() for ia in self.INTERPOLATION_AREAS]),
        )

        result = InterpolationAreaManager.readInterpolationAreasFromLayer(layer)

        assert result == self.INTERPOLATION_AREAS

    def test_givenNoneLayer_whenWritingInterpolationAreasFromLayer_thenReturnFalse(
        self,
    ):
        layer = None

        result = InterpolationAreaManager.writeInterpolationAreasOfLayer(
            layer,
            self.INTERPOLATION_AREAS,
        )

        assert result is False

    def test_givenLayer_whenWritingInterpolationAreasFromLayer_thenReturnTrueAndWriteIt(
        self,
    ):
        layer = QgsVectorLayer()

        result = InterpolationAreaManager.writeInterpolationAreasOfLayer(
            layer,
            self.INTERPOLATION_AREAS,
        )
        actualCustomProperty = layer.customProperty(
            InterpolationAreaManager.INTERPOLATION_AREAS_KEY
        )

        actualJson = json.loads(actualCustomProperty)
        expectedJson = json.loads(self.INTERPOLATION_AREAS_STR)
        assert result is True
        assert actualJson == expectedJson

    def test_givenLayer_whenWritingAndThenReading_thenComingBackToStartingValue(
        self,
    ):
        layer = QgsVectorLayer()

        result = InterpolationAreaManager.writeInterpolationAreasOfLayer(
            layer,
            self.INTERPOLATION_AREAS,
        )
        actualInterpolationAreas = (
            InterpolationAreaManager.readInterpolationAreasFromLayer(layer)
        )

        assert result is True
        assert actualInterpolationAreas == self.INTERPOLATION_AREAS

    def test_givenValidAreas_whenValidate_thenReturnOk(self):
        ok, message = InterpolationAreaManager.validateInterpolationAreasOfLayer(
            self.INTERPOLATION_AREAS,
            self.layer,
        )
        assert message == ""
        assert ok is True

    def test_givenToProfilIdLesserThanFromProfilId_whenValidate_thenReturnValidationError(
        self,
    ):
        ok, message = InterpolationAreaManager.validateInterpolationAreasOfLayer(
            [
                InterpolationArea(5, 1, 100.0),
            ],
            self.layer,
        )
        assert ok is False
        assert (
            message
            == "First area's from_profil_id (5) should be the first section id: 1"
        )

    def test_givenFromLastProfilIdIsNotRight_whenValidate_thenReturnValidationError(
        self,
    ):
        ok, message = InterpolationAreaManager.validateInterpolationAreasOfLayer(
            [
                InterpolationArea(1, 3, 100.0),
                InterpolationArea(3, 5, 100.0),
            ],
            self.layer,
        )
        assert ok is False
        assert message == "Last area's to_profil_id should be the last section id: 6"

    def test_givenFromProfilIdLowerThanPreviousToProfilId_whenValidate_thenReturnValidationError(
        self,
    ):
        ok, message = InterpolationAreaManager.validateInterpolationAreasOfLayer(
            [
                InterpolationArea(1, 5, 100.0),
                InterpolationArea(3, 6, 100.0),
            ],
            self.layer,
        )
        assert ok is False
        assert (
            message
            == "Area number 1 has a from_profil_id different than the previous area's to_profil_id."
        )
