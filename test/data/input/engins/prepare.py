#!/usr/bin/env python3

import processing
from processing import Processing

BASE_FOLDER = "/app/test/data/input/engins"

TOPO_BAT_EXPR = "'B' || replace(rpad('', num_points($geometry) - 1, ','), ',', ',B')"

Processing.initialize()

processing.run(
    "native:refactorfields",
    {
        "INPUT": (
            f"{BASE_FOLDER}/Engins_part_1/Modele_1959/Resultats"
            "/Profil_modele_1959_v2.gpkg|layername=Profil_modele_1959_v2"
        ),
        "FIELDS_MAPPING": [
            {
                "expression": '"sec_id"',
                "length": 0,
                "name": "sec_id",
                "precision": 0,
                "type": 2,
            },
            {
                "expression": '"sec_name"',
                "length": 0,
                "name": "sec_name",
                "precision": 0,
                "type": 10,
            },
            {
                "expression": '"abs_long"',
                "length": 0,
                "name": "abs_long",
                "precision": 0,
                "type": 6,
            },
            {
                "expression": '"axis_x"',
                "length": 0,
                "name": "axis_x",
                "precision": 0,
                "type": 6,
            },
            {
                "expression": '"axis_y"',
                "length": 0,
                "name": "axis_y",
                "precision": 0,
                "type": 6,
            },
            {
                "expression": "''",
                "length": 0,
                "name": "layers",
                "precision": 0,
                "type": 10,
            },
            {
                "expression": '"p_id"',
                "length": 0,
                "name": "p_id",
                "precision": 0,
                "type": 10,
            },
            {
                "expression": TOPO_BAT_EXPR,
                "length": 0,
                "name": "topo_bat",
                "precision": 0,
                "type": 10,
            },
            {
                "expression": '"abs_lat"',
                "length": 0,
                "name": "abs_lat",
                "precision": 0,
                "type": 10,
            },
            {
                "expression": '"zfond"',
                "length": 0,
                "name": "zfond",
                "precision": 0,
                "type": 10,
            },
        ],
        "OUTPUT": f"{BASE_FOLDER}/Modele_1959/Profils_modele_1959_v2_fixed.geojson",
    },
)

processing.run(
    "native:refactorfields",
    {
        "INPUT": (
            f"{BASE_FOLDER}/Engins_part_1/Modele_2014/Resultats"
            "/Profil_modele_2014_v2.gpkg|layername=Profil_modele_2014_v2"
        ),
        "FIELDS_MAPPING": [
            {
                "expression": '"sec_id"',
                "length": 0,
                "name": "sec_id",
                "precision": 0,
                "type": 2,
            },
            {
                "expression": '"sec_name"',
                "length": 0,
                "name": "sec_name",
                "precision": 0,
                "type": 10,
            },
            {
                "expression": '"sec_pos"',
                "length": 0,
                "name": "abs_long",
                "precision": 0,
                "type": 6,
            },
            {
                "expression": '"axis_x"',
                "length": 0,
                "name": "axis_x",
                "precision": 0,
                "type": 6,
            },
            {
                "expression": '"axis_y"',
                "length": 0,
                "name": "axis_y",
                "precision": 0,
                "type": 6,
            },
            {
                "expression": "''",
                "length": 0,
                "name": "layers",
                "precision": 0,
                "type": 10,
            },
            {
                "expression": '"p_id"',
                "length": 0,
                "name": "p_id",
                "precision": 0,
                "type": 10,
            },
            {
                "expression": TOPO_BAT_EXPR,
                "length": 0,
                "name": "topo_bat",
                "precision": 0,
                "type": 10,
            },
            {
                "expression": '"p_pos"',
                "length": 0,
                "name": "abs_lat",
                "precision": 0,
                "type": 10,
            },
            {
                "expression": '"p_z"',
                "length": 0,
                "name": "zfond",
                "precision": 0,
                "type": 10,
            },
        ],
        "OUTPUT": f"{BASE_FOLDER}/Modele_2014/Profil_modele_2014_v2_fixed.geojson",
    },
)

processing.run(
    "native:refactorfields",
    {
        "INPUT": (
            f"{BASE_FOLDER}/Engins_part_1/Modele_2014/Resultats"
            "/Profils_59_modele_2014_v1.gpkg|layername=Profils_59_modele_2014_v1"
        ),
        "FIELDS_MAPPING": [
            {
                "expression": '"sec_id"',
                "length": 0,
                "name": "sec_id",
                "precision": 0,
                "type": 2,
            },
            {
                "expression": '"sec_name"',
                "length": 0,
                "name": "sec_name",
                "precision": 0,
                "type": 10,
            },
            {
                "expression": '"abs_long"',
                "length": 0,
                "name": "abs_long",
                "precision": 0,
                "type": 6,
            },
            {
                "expression": '"axis_x"',
                "length": 0,
                "name": "axis_x",
                "precision": 0,
                "type": 6,
            },
            {
                "expression": '"axis_y"',
                "length": 0,
                "name": "axis_y",
                "precision": 0,
                "type": 6,
            },
            {
                "expression": "''",
                "length": 0,
                "name": "layers",
                "precision": 0,
                "type": 10,
            },
            {
                "expression": '"p_id"',
                "length": 0,
                "name": "p_id",
                "precision": 0,
                "type": 10,
            },
            {
                "expression": TOPO_BAT_EXPR,
                "length": 0,
                "name": "topo_bat",
                "precision": 0,
                "type": 10,
            },
            {
                "expression": '"abs_lat"',
                "length": 0,
                "name": "abs_lat",
                "precision": 0,
                "type": 10,
            },
            {
                "expression": '"zfond"',
                "length": 0,
                "name": "zfond",
                "precision": 0,
                "type": 10,
            },
        ],
        "OUTPUT": f"{BASE_FOLDER}/Modele_2014/Profils_59_modele_2014_v1_fixed.geojson",
    },
)

from qgis.core import QgsApplication  # noqa
from PreCourlis.processing.precourlis_provider import PreCourlisProvider  # noqa

p = PreCourlisProvider()
assert QgsApplication.processingRegistry().addProvider(p)

processing.run(
    "precourlis:import_tracks",
    {
        "TRACKS": f"{BASE_FOLDER}/Modele_1959/Profils_modele_1959_v2_fixed.geojson",
        "AXIS": f"{BASE_FOLDER}/Modele_1959/Axe_hydraulique_1959_L93.shp",
        "FIRST_SECTION_ABS_LONG": 0,
        "FIRST_AXIS_POINT_ABS_LONG": None,
        "NAME_FIELD": "sec_name",
        "LATERAL_INTERPOLATION": True,
        "DISTANCE": 3.5,
        "STRICT_DISTANCE": True,
        "DEM": (
            "/app/test/data/input/engins"
            "/Engins_part_2/Modele_1959/Topo-Bathymetrie/iterations_raster"
            "/Bathymétrie_1959_plan_georef_v1.tif"
        ),
        "DEFAULT_ELEVATION": None,
        "OUTPUT": f"{BASE_FOLDER}/Modele_1959/Profils_modele_1959_v2_lat_step_3.5m.geojson",
    },
)
