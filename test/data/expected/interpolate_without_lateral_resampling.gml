<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     gml:id="aFeatureCollection"
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ interpolate_without_lateral_resampling.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml/3.2">
  <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::2154"><gml:lowerCorner>920677.252074925 6292816.36578553</gml:lowerCorner><gml:upperCorner>921293.662204192 6294619.86057258</gml:upperCorner></gml:Envelope></gml:boundedBy>
                                                                                                                  
  <ogr:featureMember>
    <ogr:interpolate_without_lateral_resampling gml:id="interpolate_without_lateral_resampling.0">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::2154"><gml:lowerCorner>921143.84990134 6294608.2348321</gml:lowerCorner><gml:upperCorner>921183.147371432 6294619.86057258</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::2154" gml:id="interpolate_without_lateral_resampling.geom.0"><gml:posList>921183.147371432 6294619.86057258 921182.539330709 6294619.67281178 921167.855651224 6294615.38386686 921163.63 6294614.1 921159.396898743 6294612.82241365 921144.312515425 6294608.37369904 921143.84990134 6294608.2348321</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>1</ogr:sec_id>
      <ogr:sec_name>P_0.000</ogr:sec_name>
      <ogr:abs_long>0</ogr:abs_long>
      <ogr:axis_x>921163.657838907</ogr:axis_x>
      <ogr:axis_y>6294614.11269834</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id>0,1,2,3,4,5,6</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,0.6363706766268787,15.933608719422534,20.35,24.771681820078854,40.49840054135703,40.98140759831755</ogr:abs_lat>
      <ogr:zfond>257.5,257.5,247.7,247.7,247.7,257.5,257.5</ogr:zfond>
    </ogr:interpolate_without_lateral_resampling>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:interpolate_without_lateral_resampling gml:id="interpolate_without_lateral_resampling.1">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::2154"><gml:lowerCorner>921253.156257452 6294226.15493067</gml:lowerCorner><gml:upperCorner>921293.373324333 6294233.1113963</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::2154" gml:id="interpolate_without_lateral_resampling.geom.1"><gml:posList>921293.373324333 6294233.1113963 921292.759596692 6294233.005238 921277.490828661 6294230.3641538 921273.18264942 6294229.61895523 921268.876612611 6294228.87412724 921253.94393742 6294226.29117802 921253.156257452 6294226.15493067</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>2</ogr:sec_id>
      <ogr:sec_name>P_1200.000</ogr:sec_name>
      <ogr:abs_long>1200</ogr:abs_long>
      <ogr:axis_x>921273.200581068</ogr:axis_x>
      <ogr:axis_y>6294229.62205692</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id>0,1,2,3,4,5,6</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,0.6228412321836603,16.118343900241833,20.490497749727318,24.86047735345058,40.01489634689358,40.814273023808234</ogr:abs_lat>
      <ogr:zfond>257.420152237373,257.420152237373,247.62015223737293,247.62015223737293,247.62015223737293,257.420152237373,257.420152237373</ogr:zfond>
    </ogr:interpolate_without_lateral_resampling>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:interpolate_without_lateral_resampling gml:id="interpolate_without_lateral_resampling.2">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::2154"><gml:lowerCorner>921253.687119145 6293828.62939172</gml:lowerCorner><gml:upperCorner>921293.662204192 6293835.99024308</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::2154" gml:id="interpolate_without_lateral_resampling.geom.2"><gml:posList>921293.662204192 6293828.62939172 921293.062966614 6293828.73973292 921277.62867617 6293831.58174108 921273.372308703 6293832.36549146 921269.125440098 6293833.14749276 921254.78441797 6293835.78819089 921253.687119145 6293835.99024308</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>3</ogr:sec_id>
      <ogr:sec_name>P_1200.000</ogr:sec_name>
      <ogr:abs_long>1200</ogr:abs_long>
      <ogr:axis_x>921273.378312178</ogr:axis_x>
      <ogr:axis_y>6293832.364386</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id>0,1,2,3,4,5,6</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,0.6093117877928624,16.303079081225697,20.631003373104974,24.949269110550524,39.53138837616005,40.64713467282533</ogr:abs_lat>
      <ogr:zfond>257.34030447474584,257.34030447474584,247.54030447474582,247.54030447474582,247.54030447474582,257.34030447474584,257.34030447474584</ogr:zfond>
    </ogr:interpolate_without_lateral_resampling>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:interpolate_without_lateral_resampling gml:id="interpolate_without_lateral_resampling.3">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::2154"><gml:lowerCorner>921209.595887581 6293630.28461158</gml:lowerCorner><gml:upperCorner>921248.801655261 6293640.68861871</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::2154" gml:id="interpolate_without_lateral_resampling.geom.3"><gml:posList>921248.801655261 6293630.28461158 921248.219378378 6293630.43933355 921232.954811611 6293634.49377921 921228.793500653 6293635.59907158 921224.645175803 6293636.70091469 921210.830924434 6293640.3701398 921209.595887581 6293640.68861871</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>4</ogr:sec_id>
      <ogr:sec_name>P_1001.911</ogr:sec_name>
      <ogr:abs_long>1001.91065252803</ogr:abs_long>
      <ogr:axis_x>921228.793500653</ogr:axis_x>
      <ogr:axis_y>6293635.59907158</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id>1,2,3,4,5,6,7</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,0.6024825777371814,16.39632720975833,20.701925903373155,24.994088213893622,39.28732975563161,40.562768834506095</ogr:abs_lat>
      <ogr:zfond>257.3,257.3,247.5,247.5,247.5,257.3,257.3</ogr:zfond>
    </ogr:interpolate_without_lateral_resampling>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:interpolate_without_lateral_resampling gml:id="interpolate_without_lateral_resampling.4">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::2154"><gml:lowerCorner>921068.244164494 6293253.44926238</gml:lowerCorner><gml:upperCorner>921105.301796601 6293271.27564878</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::2154" gml:id="interpolate_without_lateral_resampling.geom.4"><gml:posList>921105.301796601 6293253.44926238 921104.212946479 6293253.97304822 921090.415416826 6293260.61028004 921086.675460975 6293262.40936688 921082.810521228 6293264.26857658 921069.662641536 6293270.59329758 921068.244164494 6293271.27564878</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>5</ogr:sec_id>
      <ogr:sec_name>P_1200.000</ogr:sec_name>
      <ogr:abs_long>1200</ogr:abs_long>
      <ogr:axis_x>921086.675460975</ogr:axis_x>
      <ogr:axis_y>6293262.40936688</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id>1,2,3,4,5,6,7</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,1.2082823322765295,16.519215378236797,20.66939407559705,24.958265719758625,39.54829094376932,41.12235583511634</ogr:abs_lat>
      <ogr:zfond>257.2196372692692,257.2196372692692,247.4196372692692,247.4196372692692,247.4209298155753,257.2196372692692,257.2196372692692</ogr:zfond>
    </ogr:interpolate_without_lateral_resampling>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:interpolate_without_lateral_resampling gml:id="interpolate_without_lateral_resampling.5">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::2154"><gml:lowerCorner>920834.180050716 6292929.65373205</gml:lowerCorner><gml:upperCorner>920860.540093381 6292961.94200406</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::2154" gml:id="interpolate_without_lateral_resampling.geom.5"><gml:posList>920860.540093381 6292929.65373205 920859.392851243 6292931.05898267 920850.015474573 6292942.54528004 920847.489152558 6292945.63975805 920844.77891187 6292948.95951703 920835.364357481 6292960.49135319 920834.180050716 6292961.94200406</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>6</ogr:sec_id>
      <ogr:sec_name>P_1200.000</ogr:sec_name>
      <ogr:abs_long>1200</ogr:abs_long>
      <ogr:axis_x>920847.489152558</ogr:axis_x>
      <ogr:axis_y>6292945.63975805</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id>1,2,3,4,5,6,7</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,1.8140820875779604,16.64210354763663,20.636862248312593,24.92244322620501,39.809252131883795,41.68194283643864</ogr:abs_lat>
      <ogr:zfond>257.13927453853836,257.13927453853836,247.33927453853835,247.33927453853835,247.34185963115058,257.13927453853836,257.13927453853836</ogr:zfond>
    </ogr:interpolate_without_lateral_resampling>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:interpolate_without_lateral_resampling gml:id="interpolate_without_lateral_resampling.6">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::2154"><gml:lowerCorner>920677.252074925 6292816.36578553</gml:lowerCorner><gml:upperCorner>920696.57637653 6292853.60583991</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::2154" gml:id="interpolate_without_lateral_resampling.geom.6"><gml:posList>920696.57637653 6292816.36578553 920695.600124583 6292818.23652024 920688.858584247 6292831.17787 920687.04808959 6292834.65337313 920685.068885524 6292838.45273808 920678.175744741 6292851.81092577 920677.252074925 6292853.60583991</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>7</ogr:sec_id>
      <ogr:sec_name>P_1997.397</ogr:sec_name>
      <ogr:abs_long>1997.39698397145</ogr:abs_long>
      <ogr:axis_x>920687.04808959</ogr:axis_x>
      <ogr:axis_y>6292834.65337313</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id>1,2,3,4,5,6,7</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,2.110146016090794,16.70216094011572,20.620963428846622,24.904936206207836,39.93678799494121,41.95542186059127</ogr:abs_lat>
      <ogr:zfond>257.1,257.1,247.3,247.3,247.30321678045127,257.1,257.1</ogr:zfond>
    </ogr:interpolate_without_lateral_resampling>
  </ogr:featureMember>
</ogr:FeatureCollection>
