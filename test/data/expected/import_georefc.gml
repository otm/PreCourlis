<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     gml:id="aFeatureCollection"
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ import_georefc.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml/3.2">
  <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::2154"><gml:lowerCorner>425952.006135 244648.444785</gml:lowerCorner><gml:upperCorner>427870.693252 247351.955985</gml:upperCorner></gml:Envelope></gml:boundedBy>
                                                                                                                              
  <ogr:featureMember>
    <ogr:import_georefc gml:id="import_georefc.0">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::2154"><gml:lowerCorner>427843.745399 244648.444785</gml:lowerCorner><gml:upperCorner>427870.693252 245424.542945</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::2154" gml:id="import_georefc.geom.0"><gml:posList>427843.745399 244648.444785 427847.21553 244748.384558 427850.685661 244848.324331 427854.155792 244948.264104 427857.625923 245048.203877 427861.096054 245148.143649 427864.566185 245248.083422 427868.036316 245348.023195 427870.693252 245424.542945</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>1</ogr:sec_id>
      <ogr:sec_name>P1</ogr:sec_name>
      <ogr:abs_long>0</ogr:abs_long>
      <ogr:axis_x>427858.100666</ogr:axis_x>
      <ogr:axis_y>245061.876469</ogr:axis_y>
      <ogr:layers>Layer_0</ogr:layers>
      <ogr:p_id>0,1,2,3,4,5,6,7,8</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,100.0,200.0,300.0,400.0,500.0,600.0,700.0,776.565863</ogr:abs_lat>
      <ogr:zfond>22.411903,22.00436,21.560755,20.649473,10.974686,10.980537,20.306419,16.12434,12.896034</ogr:zfond>
      <ogr:Layer_0>21.411903,21.00436,20.560755,19.649473,9.974686,9.980537,19.306419,15.12434,11.896034</ogr:Layer_0>
    </ogr:import_georefc>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:import_georefc gml:id="import_georefc.1">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::2154"><gml:lowerCorner>427353.294479 244691.56135</gml:lowerCorner><gml:upperCorner>427466.47546 245338.309816</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::2154" gml:id="import_georefc.geom.1"><gml:posList>427353.294479 244691.56135 427370.532512 244790.064396 427387.770545 244888.567443 427405.008578 244987.07049 427422.246611 245085.573537 427439.484644 245184.076583 427456.722678 245282.57963 427466.47546 245338.309816</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>2</ogr:sec_id>
      <ogr:sec_name>P2</ogr:sec_name>
      <ogr:abs_long>451.308572</ogr:abs_long>
      <ogr:axis_x>427409.701838</ogr:axis_x>
      <ogr:axis_y>245013.889116</ogr:axis_y>
      <ogr:layers>Layer_0</ogr:layers>
      <ogr:p_id>0,1,2,3,4,5,6,7</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,100.0,200.0,300.0,400.0,500.0,600.0,656.577119</ogr:abs_lat>
      <ogr:zfond>19.53274,19.496868,16.067188,11.535794,11.904032,17.562712,12.613772,12.6259</ogr:zfond>
      <ogr:Layer_0>18.53274,18.496868,15.067188,10.535794,10.904032,16.562712,11.613772,11.6259</ogr:Layer_0>
    </ogr:import_georefc>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:import_georefc gml:id="import_georefc.2">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::2154"><gml:lowerCorner>426641.871166 244934.092025</gml:lowerCorner><gml:upperCorner>426895.180982 245499.996933</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::2154" gml:id="import_georefc.geom.2"><gml:posList>426641.871166 244934.092025 426707.851283 245009.236047 426773.8314 245084.38007 426836.901015 245160.913489 426853.840136 245259.468378 426870.779258 245358.023267 426887.718379 245456.578156 426895.180982 245499.996933</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>3</ogr:sec_id>
      <ogr:sec_name>P3</ogr:sec_name>
      <ogr:abs_long>1063.865951</ogr:abs_long>
      <ogr:axis_x>426836.804291</ogr:axis_x>
      <ogr:axis_y>245160.35073</ogr:axis_y>
      <ogr:layers>Layer_0</ogr:layers>
      <ogr:p_id>0,1,2,3,4,5,6,7</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,100.0,200.0,299.172277,399.172277,499.172277,599.172277,643.227703</ogr:abs_lat>
      <ogr:zfond>22.130899,21.038624,17.553612,11.429041,20.847527,19.428787,18.064766,17.591026</ogr:zfond>
      <ogr:Layer_0>21.130899,20.038624,16.553612,10.429041,19.847527,18.428787,17.064766,16.591026</ogr:Layer_0>
    </ogr:import_georefc>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:import_georefc gml:id="import_georefc.3">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::2154"><gml:lowerCorner>425952.006135 245607.788344</gml:lowerCorner><gml:upperCorner>426954.466258 246028.174847</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::2154" gml:id="import_georefc.geom.3"><gml:posList>425952.006135 245607.788344 426035.417561 245662.947513 426118.828988 245718.106682 426202.240414 245773.265851 426285.651841 245828.42502 426385.292195 245821.594518 426485.031929 245814.384417 426584.771662 245807.174316 426684.511396 245799.964214 426768.611601 245833.254109 426837.619134 245905.627863 426906.626667 245978.001618 426954.466258 246028.174847</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>4</ogr:sec_id>
      <ogr:sec_name>P4</ogr:sec_name>
      <ogr:abs_long>1840.744533</ogr:abs_long>
      <ogr:axis_x>426505.775416</ogr:axis_x>
      <ogr:axis_y>245812.884887</ogr:axis_y>
      <ogr:layers>Layer_0</ogr:layers>
      <ogr:p_id>0,1,2,3,4,5,6,7,8,9,10,11,12</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,100.0,200.0,300.0,400.0,499.874201,599.874201,699.874201,799.874201,890.323422,990.323422,1090.323422,1159.648592</ogr:abs_lat>
      <ogr:zfond>20.029297,20.0,21.291914,22.178438,22.159451,21.155355,10.067788,20.893515,19.469173,18.194504,17.405169,16.607012,17.029566</ogr:zfond>
      <ogr:Layer_0>19.029297,19.0,20.291914,21.178438,21.159451,20.155355,9.067788,19.893515,18.469173,17.194504,16.405169,15.607012,16.029566</ogr:Layer_0>
    </ogr:import_georefc>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:import_georefc gml:id="import_georefc.4">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::2154"><gml:lowerCorner>425993.233374 246286.349743</gml:lowerCorner><gml:upperCorner>427158.7402 247351.955985</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::2154" gml:id="import_georefc.geom.4"><gml:posList>425993.233374 246286.349743 426067.036255 246353.826663 426140.839136 246421.303583 426214.642017 246488.780503 426288.444898 246556.257423 426362.24778 246623.734343 426436.050661 246691.211263 426509.853542 246758.688183 426583.656423 246826.165103 426657.459304 246893.642023 426731.262186 246961.118943 426805.065067 247028.595863 426878.867948 247096.072783 426952.670829 247163.549703 427026.47371 247231.026623 427100.276592 247298.503543 427158.7402 247351.955985</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>5</ogr:sec_id>
      <ogr:sec_name>P5</ogr:sec_name>
      <ogr:abs_long>2826.7468</ogr:abs_long>
      <ogr:axis_x>426473.273607</ogr:axis_x>
      <ogr:axis_y>246725.243671</ogr:axis_y>
      <ogr:layers>Layer_0</ogr:layers>
      <ogr:p_id>0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B,B,B,B,B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,100.0,200.0,300.0,400.0,500.0,600.0,700.0,800.0,900.0,1000.0,1100.0,1200.0,1300.0,1400.0,1500.0,1579.215889</ogr:abs_lat>
      <ogr:zfond>21.359831,20.918606,21.088734,21.495714,20.77939,19.928963,14.023784,7.394282,20.321514,22.887957,24.128172,25.388321,27.457115,28.711273,nan,nan,nan</ogr:zfond>
      <ogr:Layer_0>20.359831,19.918606,20.088734,20.495714,19.77939,18.928963,13.023784,6.394282,19.321514,21.887957,23.128172,24.388321,26.457115,27.711273,nan,nan,nan</ogr:Layer_0>
    </ogr:import_georefc>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:import_georefc gml:id="import_georefc.5">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::2154"><gml:lowerCorner>426109.784056 246611.026645</gml:lowerCorner><gml:upperCorner>426471.923677 247081.3919</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::2154" gml:id="import_georefc.geom.5"><gml:posList>426109.784056 246611.026645 426170.789014 246690.262969 426231.793972 246769.499294 426292.79893 246848.735618 426353.803887 246927.971943 426414.808845 247007.208267 426471.923677 247081.3919</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>6</ogr:sec_id>
      <ogr:sec_name>P6</ogr:sec_name>
      <ogr:abs_long>3045.155137</ogr:abs_long>
      <ogr:axis_x>426296.882708</ogr:axis_x>
      <ogr:axis_y>246854.039836</ogr:axis_y>
      <ogr:layers>Layer_0</ogr:layers>
      <ogr:p_id>0,1,2,3,4,5,6</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,100.0,200.0,300.0,400.0,500.0,593.623263</ogr:abs_lat>
      <ogr:zfond>21.061501,20.745626,19.653952,8.563537,12.578148,23.255896,25.198254</ogr:zfond>
      <ogr:Layer_0>20.061501,19.745626,18.653952,7.563537,11.578148,22.255896,24.198254</ogr:Layer_0>
    </ogr:import_georefc>
  </ogr:featureMember>
</ogr:FeatureCollection>
