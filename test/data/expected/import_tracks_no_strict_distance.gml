<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     gml:id="aFeatureCollection"
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ import_tracks_no_strict_distance.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml/3.2">
  <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::27563"><gml:lowerCorner>425952.006134969 244648.444785276</gml:lowerCorner><gml:upperCorner>427870.693251534 247351.955984703</gml:upperCorner></gml:Envelope></gml:boundedBy>
                                                                                                                 
  <ogr:featureMember>
    <ogr:import_tracks_no_strict_distance gml:id="import_tracks_no_strict_distance.0">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::27563"><gml:lowerCorner>427843.745398773 244648.444785276</gml:lowerCorner><gml:upperCorner>427870.693251534 245424.542944785</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::27563" gml:id="import_tracks_no_strict_distance.geom.0"><gml:posList>427843.745398773 244648.444785276 427847.113880368 244745.457055215 427850.482361963 244842.469325153 427853.850843558 244939.481595092 427857.219325153 245036.493865031 427860.587806749 245133.506134969 427863.956288344 245230.518404908 427867.324769939 245327.530674847 427870.693251534 245424.542944785</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>1</ogr:sec_id>
      <ogr:sec_name>P1</ogr:sec_name>
      <ogr:abs_long>0</ogr:abs_long>
      <ogr:axis_x>427858.100665553</ogr:axis_x>
      <ogr:axis_y>245061.876468527</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id>1,2,3,4,5,6,7,8,9</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,97.07073290599445,194.1414658119909,291.21219871798536,388.28293162395073,485.3536645299472,582.4243974359416,679.4951303419381,776.5658632479325</ogr:abs_lat>
      <ogr:zfond>22.411904,22.004361,21.600802,21.253284,11.600077,11.143404,17.218299,17.041975,12.896034</ogr:zfond>
    </ogr:import_tracks_no_strict_distance>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:import_tracks_no_strict_distance gml:id="import_tracks_no_strict_distance.1">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::27563"><gml:lowerCorner>427353.294478528 244691.561349693</gml:lowerCorner><gml:upperCorner>427466.475460123 245338.309815951</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::27563" gml:id="import_tracks_no_strict_distance.geom.1"><gml:posList>427353.294478528 244691.561349693 427369.463190184 244783.95398773 427385.631901841 244876.346625767 427401.800613497 244968.739263804 427417.969325153 245061.13190184 427434.13803681 245153.524539877 427450.306748466 245245.917177914 427466.475460123 245338.309815951</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>2</ogr:sec_id>
      <ogr:sec_name>P2</ogr:sec_name>
      <ogr:abs_long>451.308571519184</ogr:abs_long>
      <ogr:axis_x>427409.701837606</ogr:axis_x>
      <ogr:axis_y>245013.889115853</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id>1,2,3,4,5,6,7,8</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,93.7967312864825,187.59346257297503,281.39019385945755,375.1869251459501,468.9836564324326,562.7803877189251,656.5771190054077</ogr:abs_lat>
      <ogr:zfond>19.532739,19.498223,19.554515,11.50934,11.600044,14.017806,14.555969,12.6259</ogr:zfond>
    </ogr:import_tracks_no_strict_distance>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:import_tracks_no_strict_distance gml:id="import_tracks_no_strict_distance.2">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::27563"><gml:lowerCorner>426641.871165644 244934.09202454</gml:lowerCorner><gml:upperCorner>426895.180981595 245499.996932515</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::27563" gml:id="import_tracks_no_strict_distance.geom.2"><gml:posList>426641.871165644 244934.09202454 426706.54601227 245007.749488753 426771.220858896 245081.406952965 426835.895705522 245155.064417178 426850.71702454 245241.297546012 426865.538343558 245327.530674847 426880.359662577 245413.763803681 426895.180981595 245499.996932515</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>3</ogr:sec_id>
      <ogr:sec_name>P3</ogr:sec_name>
      <ogr:abs_long>1063.86595130483</ogr:abs_long>
      <ogr:axis_x>426836.804290604</ogr:axis_x>
      <ogr:axis_y>245160.350730384</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id>1,2,3,4,5,6,7,8</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,98.02172116581677,196.04344233163354,294.0651634974503,381.56272921222467,469.0602949269604,556.5578606416962,644.0554263564704</ogr:abs_lat>
      <ogr:zfond>22.1309,21.038624,20.038045,11.429041,18.15979,19.331726,18.684976,17.591027</ogr:zfond>
    </ogr:import_tracks_no_strict_distance>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:import_tracks_no_strict_distance gml:id="import_tracks_no_strict_distance.3">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::27563"><gml:lowerCorner>425952.006134969 245607.788343558</gml:lowerCorner><gml:upperCorner>426954.466257669 246028.174846626</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::27563" gml:id="import_tracks_no_strict_distance.geom.3"><gml:posList>425952.006134969 245607.788343558 426018.836809816 245651.982822086 426085.667484663 245696.177300613 426152.498159509 245740.371779141 426219.328834356 245784.566257669 426286.159509202 245828.760736196 426375.626380368 245822.293251534 426465.093251534 245815.825766871 426554.560122699 245809.358282209 426644.026993865 245802.890797546 426733.493865031 245796.423312883 426788.73696319 245854.361196319 426843.98006135 245912.299079755 426899.223159509 245970.23696319 426954.466257669 246028.174846626</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>4</ogr:sec_id>
      <ogr:sec_name>P4</ogr:sec_name>
      <ogr:abs_long>1840.74453316491</ogr:abs_long>
      <ogr:axis_x>426505.77541644</ogr:axis_x>
      <ogr:axis_y>245812.88488748</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id>1,2,3,4,5,6,7,8,9,10,11,12,13,14,15</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B,B,B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,80.12172634672488,160.24345269349834,240.36517904020718,320.48690538698065,400.6086317337055,490.3089628035253,580.009293873287,669.7096249431088,759.4099560128705,849.1102870826902,929.1640079909264,1009.2177288991626,1089.2714498074602,1169.3251707156965</ogr:abs_lat>
      <ogr:zfond>20.029296,20.0,20.800774,21.843452,22.200006,22.15945,21.283105,9.4763588,12.558612,20.059866,18.730722,17.837025,17.275156,16.765841,17.029565</ogr:zfond>
    </ogr:import_tracks_no_strict_distance>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:import_tracks_no_strict_distance gml:id="import_tracks_no_strict_distance.4">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::27563"><gml:lowerCorner>425993.233373578 246286.349743189</gml:lowerCorner><gml:upperCorner>427158.740200234 247351.955984703</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::27563" gml:id="import_tracks_no_strict_distance.geom.4"><gml:posList>425993.233373578 246286.349743189 426066.077550244 246352.950133284 426138.92172691 246419.550523378 426211.765903576 246486.150913473 426284.610080242 246552.751303568 426357.454256908 246619.351693662 426430.298433574 246685.952083757 426503.14261024 246752.552473851 426575.986786906 246819.152863946 426648.830963572 246885.753254041 426721.675140238 246952.353644135 426794.519316904 247018.95403423 426867.36349357 247085.554424324 426940.207670236 247152.154814419 427013.051846902 247218.755204514 427085.896023568 247285.355594608 427158.740200234 247351.955984703</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>5</ogr:sec_id>
      <ogr:sec_name>P5</ogr:sec_name>
      <ogr:abs_long>2826.74680044045</ogr:abs_long>
      <ogr:axis_x>426473.273606768</ogr:axis_x>
      <ogr:axis_y>246725.243670677</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id>1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B,B,B,B,B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,98.70099307959877,197.4019861591742,296.102979238773,394.8039723183288,493.5049653979276,592.2059584774834,690.9069515571018,789.6079446367006,888.3089377162564,987.0099307958749,1085.7109238754306,1184.4119169550293,1283.1129100345852,1381.8139031141839,1480.5148961937593,1579.215889273358</ogr:abs_lat>
      <ogr:zfond>21.35983,20.918605,21.088734,21.449916,20.849767,19.983695,15.777539,7.900693,19.988927,22.770503,24.136239,25.103795,27.095471,28.550945,NULL,NULL,NULL</ogr:zfond>
    </ogr:import_tracks_no_strict_distance>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:import_tracks_no_strict_distance gml:id="import_tracks_no_strict_distance.5">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::27563"><gml:lowerCorner>426109.784056244 246611.0266449</gml:lowerCorner><gml:upperCorner>426471.923677383 247081.391899944</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::27563" gml:id="import_tracks_no_strict_distance.geom.5"><gml:posList>426109.784056244 246611.0266449 426170.140659767 246689.420854074 426230.49726329 246767.815063248 426290.853866813 246846.209272422 426351.210470337 246924.603481596 426411.56707386 247002.99769077 426471.923677383 247081.391899944</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id>6</ogr:sec_id>
      <ogr:sec_name>P6</ogr:sec_name>
      <ogr:abs_long>3045.15513690845</ogr:abs_long>
      <ogr:axis_x>426296.882707939</ogr:axis_x>
      <ogr:axis_y>246854.039836183</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id>1,2,3,4,5,6,7</ogr:p_id>
      <ogr:topo_bat>B,B,B,B,B,B,B</ogr:topo_bat>
      <ogr:abs_lat>0.0,98.93721049664458,197.87442099325364,296.81163148987514,395.7488419865197,494.6860524831288,593.6232629797734</ogr:abs_lat>
      <ogr:zfond>21.0615,20.745627,19.653951,8.633092,10.265055,22.971371,25.198254</ogr:zfond>
    </ogr:import_tracks_no_strict_distance>
  </ogr:featureMember>
</ogr:FeatureCollection>
