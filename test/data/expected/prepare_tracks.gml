<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     gml:id="aFeatureCollection"
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ prepare_tracks.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml/3.2">
  <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::27563"><gml:lowerCorner>425952.006134969 244648.444785276</gml:lowerCorner><gml:upperCorner>427870.693251534 247351.955984703</gml:upperCorner></gml:Envelope></gml:boundedBy>
                                                                                                                 
  <ogr:featureMember>
    <ogr:prepare_tracks gml:id="prepare_tracks.0">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::27563"><gml:lowerCorner>425952.006134969 245607.788343558</gml:lowerCorner><gml:upperCorner>426954.466257669 246028.174846626</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiCurve srsName="urn:ogc:def:crs:EPSG::27563" gml:id="prepare_tracks.geom.0"><gml:curveMember><gml:LineString gml:id="prepare_tracks.geom.0.0"><gml:posList>425952.006134969 245607.788343558 426286.159509202 245828.760736196 426733.493865031 245796.423312883 426954.466257669 246028.174846626</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></ogr:geometryProperty>
      <ogr:sec_id xsi:nil="true"/>
      <ogr:sec_name>P4</ogr:sec_name>
      <ogr:abs_long>2043.04443611251</ogr:abs_long>
      <ogr:axis_x>426505.77541644</ogr:axis_x>
      <ogr:axis_y>245812.88488748</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id xsi:nil="true"/>
      <ogr:topo_bat>B</ogr:topo_bat>
      <ogr:abs_lat xsi:nil="true"/>
      <ogr:zfond xsi:nil="true"/>
    </ogr:prepare_tracks>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:prepare_tracks gml:id="prepare_tracks.1">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::27563"><gml:lowerCorner>426641.871165644 244934.09202454</gml:lowerCorner><gml:upperCorner>426895.180981595 245499.996932515</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiCurve srsName="urn:ogc:def:crs:EPSG::27563" gml:id="prepare_tracks.geom.1"><gml:curveMember><gml:LineString gml:id="prepare_tracks.geom.1.0"><gml:posList>426641.871165644 244934.09202454 426835.895705522 245155.064417178 426895.180981595 245499.996932515</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></ogr:geometryProperty>
      <ogr:sec_id xsi:nil="true"/>
      <ogr:sec_name>P3</ogr:sec_name>
      <ogr:abs_long>1266.16585425243</ogr:abs_long>
      <ogr:axis_x>426836.804290604</ogr:axis_x>
      <ogr:axis_y>245160.350730384</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id xsi:nil="true"/>
      <ogr:topo_bat>B</ogr:topo_bat>
      <ogr:abs_lat xsi:nil="true"/>
      <ogr:zfond xsi:nil="true"/>
    </ogr:prepare_tracks>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:prepare_tracks gml:id="prepare_tracks.2">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::27563"><gml:lowerCorner>427353.294478528 244691.561349693</gml:lowerCorner><gml:upperCorner>427466.475460123 245338.309815951</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiCurve srsName="urn:ogc:def:crs:EPSG::27563" gml:id="prepare_tracks.geom.2"><gml:curveMember><gml:LineString gml:id="prepare_tracks.geom.2.0"><gml:posList>427353.294478528 244691.561349693 427466.475460123 245338.309815951</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></ogr:geometryProperty>
      <ogr:sec_id xsi:nil="true"/>
      <ogr:sec_name>P2</ogr:sec_name>
      <ogr:abs_long>653.608474466787</ogr:abs_long>
      <ogr:axis_x>427409.701837606</ogr:axis_x>
      <ogr:axis_y>245013.889115853</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id xsi:nil="true"/>
      <ogr:topo_bat>B</ogr:topo_bat>
      <ogr:abs_lat xsi:nil="true"/>
      <ogr:zfond xsi:nil="true"/>
    </ogr:prepare_tracks>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:prepare_tracks gml:id="prepare_tracks.3">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::27563"><gml:lowerCorner>427843.745398773 244648.444785276</gml:lowerCorner><gml:upperCorner>427870.693251534 245424.542944785</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiCurve srsName="urn:ogc:def:crs:EPSG::27563" gml:id="prepare_tracks.geom.3"><gml:curveMember><gml:LineString gml:id="prepare_tracks.geom.3.0"><gml:posList>427843.745398773 244648.444785276 427870.693251534 245424.542944785</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></ogr:geometryProperty>
      <ogr:sec_id xsi:nil="true"/>
      <ogr:sec_name>P1</ogr:sec_name>
      <ogr:abs_long>202.299902947603</ogr:abs_long>
      <ogr:axis_x>427858.100665553</ogr:axis_x>
      <ogr:axis_y>245061.876468527</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id xsi:nil="true"/>
      <ogr:topo_bat>B</ogr:topo_bat>
      <ogr:abs_lat xsi:nil="true"/>
      <ogr:zfond xsi:nil="true"/>
    </ogr:prepare_tracks>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:prepare_tracks gml:id="prepare_tracks.4">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::27563"><gml:lowerCorner>425993.233373578 246286.349743189</gml:lowerCorner><gml:upperCorner>427158.740200234 247351.955984703</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::27563" gml:id="prepare_tracks.geom.4"><gml:posList>425993.233373578 246286.349743189 427158.740200234 247351.955984703</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id xsi:nil="true"/>
      <ogr:sec_name>P5</ogr:sec_name>
      <ogr:abs_long>3029.04670338805</ogr:abs_long>
      <ogr:axis_x>426473.273606768</ogr:axis_x>
      <ogr:axis_y>246725.243670677</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id xsi:nil="true"/>
      <ogr:topo_bat>B</ogr:topo_bat>
      <ogr:abs_lat xsi:nil="true"/>
      <ogr:zfond xsi:nil="true"/>
    </ogr:prepare_tracks>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:prepare_tracks gml:id="prepare_tracks.5">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::27563"><gml:lowerCorner>426109.784056244 246611.0266449</gml:lowerCorner><gml:upperCorner>426471.923677383 247081.391899944</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:LineString srsName="urn:ogc:def:crs:EPSG::27563" gml:id="prepare_tracks.geom.5"><gml:posList>426109.784056244 246611.0266449 426471.923677383 247081.391899944</gml:posList></gml:LineString></ogr:geometryProperty>
      <ogr:sec_id xsi:nil="true"/>
      <ogr:sec_name>P6</ogr:sec_name>
      <ogr:abs_long>3247.45503985605</ogr:abs_long>
      <ogr:axis_x>426296.882707939</ogr:axis_x>
      <ogr:axis_y>246854.039836183</ogr:axis_y>
      <ogr:layers></ogr:layers>
      <ogr:p_id xsi:nil="true"/>
      <ogr:topo_bat>B</ogr:topo_bat>
      <ogr:abs_lat xsi:nil="true"/>
      <ogr:zfond xsi:nil="true"/>
    </ogr:prepare_tracks>
  </ogr:featureMember>
</ogr:FeatureCollection>
