import os

from .. import DATA_PATH
from . import TestCase

PROFILES_PATH = os.path.join(DATA_PATH, "input", "canal", "profiles.geojson")
AXIS_PATH = os.path.join(DATA_PATH, "input", "canal", "Axe_Canal_Long_lg.shp")


class TestInterpolateProfilesWithoutLateralResamplingAlgorithm(TestCase):
    ALGORITHM_ID = "precourlis:interpolate_without_lateral_resampling"
    DEFAULT_PARAMS = {
        "SECTIONS": PROFILES_PATH,
        "AXIS": AXIS_PATH,
        "LONG_STEP": 400.0,
        "ATTR_CROSS_SECTION": "sec_id",
    }

    def test_interpolate_without_lateral_resampling(self):
        self.check_algorithm(
            {},
            {
                "OUTPUT": "interpolate_without_lateral_resampling.gml",
            },
        )
