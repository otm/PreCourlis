import os

from .. import (
    DATA_PATH,
)
from . import TestCase

PROLFILES_2_0 = os.path.join(
    DATA_PATH,
    "input",
    "engins",
    "Modele_1959",
    "Profils_modele_1959_v2.geojson",
)


class TestRepairProfilesAlgorithm(TestCase):
    ALGORITHM_ID = "precourlis:repair_profiles"
    DEFAULT_PARAMS = {
        "INPUT": PROLFILES_2_0,
    }

    def test_algorithm_success(self):
        self.check_algorithm({}, {"OUTPUT": "repair_profiles.gml"})
