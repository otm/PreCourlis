import os

from .. import DATA_PATH
from . import TestCase

PROJECT_PATH = os.path.join(DATA_PATH, "input", "Projection_points_traces")
TARGET_PROFILES_PATH = os.path.join(PROJECT_PATH, "profiles.geojson")
SOURCE_POINTS_PATH = os.path.join(PROJECT_PATH, "raw_points.geojson")


class TestJoinPointsToProfilesAlgorithm(TestCase):

    ALGORITHM_ID = "precourlis:join_points_to_profiles"
    DEFAULT_PARAMS = {
        "SOURCE_POINTS": SOURCE_POINTS_PATH,
        "TARGET_PROFILES": TARGET_PROFILES_PATH,
        "TARGET_PROFILES_FIELD_SEC_ID": "sec_name",
        "BUFFER_DISTANCE": 1.0,
    }

    def test_join_points_to_profiles(self):
        self.check_algorithm({}, {"OUTPUT": "join_points_to_profiles.gml"})
