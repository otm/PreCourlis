import os

from qgis.core import QgsVectorLayer

from PreCourlis.core.precourlis_file import PreCourlisFileLine

from .. import (
    DATA_PATH,
    EXPECTED_PATH,
    OUTPUT_PATH,
    OVERWRITE_EXPECTED,
)
from ..utils import save_as_gml
from . import TestCase

ENGIN_INPUT = os.path.join(DATA_PATH, "input", "engins")
FOLDER_1959 = os.path.join(ENGIN_INPUT, "Modele_1959")
FOLDER_2014 = os.path.join(ENGIN_INPUT, "Modele_2014")

PROFILES_1959 = os.path.join(FOLDER_1959, "Profils_modele_1959_v2_fixed.geojson")
PROFILES_1959_LAT_STEP_3_5 = os.path.join(
    FOLDER_1959, "Profils_modele_1959_v2_lat_step_3.5m.geojson"
)
PROFILES_2014_V1 = os.path.join(FOLDER_2014, "Profils_59_modele_2014_v1_fixed.geojson")
PROFILES_2014_V2 = os.path.join(FOLDER_2014, "Profils_modele_2014_v2_fixed.geojson")
MASK = os.path.join(ENGIN_INPUT, "mask.geojson")


class TestImportLayerFromProfilesAlgorithm(TestCase):

    ALGORITHM_ID = "precourlis:import_layer_from_profiles"

    def test_import_layer_from_profiles_similar_points(self):
        layer = QgsVectorLayer(PROFILES_2014_V1)
        file = PreCourlisFileLine(layer)
        file.add_sedimental_layer("1959", from_layer="zfond", deltaz=-1)

        self.check_algorithm(
            {
                "TARGET_PROFILES": layer,
                "TARGET_LAYER_NAME": "1959",
                "SOURCE_PROFILES": PROFILES_1959,
                "SOURCE_FIELD": "zfond",
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(
            EXPECTED_PATH, "import_layer_from_profiles_similar_points.gml"
        )
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(
                OUTPUT_PATH, "import_layer_from_profiles_similar_points.gml"
            )
        save_as_gml(layer, output)

        self.compare_layers(expected, output)

    def test_import_layer_from_profiles_create_new_layer(self):
        layer = QgsVectorLayer(PROFILES_2014_V1)

        self.check_algorithm(
            {
                "TARGET_PROFILES": layer,
                "TARGET_LAYER_NAME": "1959",
                "SOURCE_PROFILES": PROFILES_1959,
                "SOURCE_FIELD": "zfond",
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(
            EXPECTED_PATH, "import_layer_from_profiles_create_new_layer.gml"
        )
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(
                OUTPUT_PATH, "import_layer_from_profiles_create_new_layer.gml"
            )
        save_as_gml(layer, output)

        self.compare_layers(expected, output)

    # def test_import_layer_from_profiles_with_default_elevation(self):
    #     layer = QgsVectorLayer(PROFILE_LINES_PATH)

    #     self.check_algorithm(
    #         {
    #             "TARGET_PROFILES": layer,
    #             "DEFAULT_ELEVATION": 100.0,
    #         }
    #     )

    #     # Copy temporary layer to output
    #     expected = os.path.join(
    #         EXPECTED_PATH, "import_layer_from_profiles_with_default_elevation.gml"
    #     )
    #     if OVERWRITE_EXPECTED:
    #         output = expected
    #     else:
    #         output = os.path.join(
    #             OUTPUT_PATH, "import_layer_from_profiles_with_default_elevation.gml"
    #         )
    #     save_as_gml(layer, output)

    #     self.compare_layers(expected, output)

    def test_import_layer_from_profiles_with_interpolation(self):
        layer = QgsVectorLayer(PROFILES_2014_V1)
        file = PreCourlisFileLine(layer)
        file.add_sedimental_layer("1959", from_layer="zfond", deltaz=-1)

        self.check_algorithm(
            {
                "TARGET_PROFILES": layer,
                "TARGET_LAYER_NAME": "1959",
                "SOURCE_PROFILES": PROFILES_1959_LAT_STEP_3_5,
                "SOURCE_FIELD": "zfond",
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(
            EXPECTED_PATH, "import_layer_from_profiles_with_interpolation.gml"
        )
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(
                OUTPUT_PATH, "import_layer_from_profiles_with_interpolation.gml"
            )
        save_as_gml(layer, output)

        self.compare_layers(expected, output)

    def test_import_layer_from_profiles_with_points_filter(self):
        layer = QgsVectorLayer(PROFILES_2014_V1)
        file = PreCourlisFileLine(layer)
        file.add_sedimental_layer("1959", from_layer="zfond", deltaz=-1)

        self.check_algorithm(
            {
                "TARGET_PROFILES": layer,
                "TARGET_LAYER_NAME": "1959",
                "SOURCE_PROFILES": PROFILES_1959,
                "SOURCE_FIELD": "zfond",
                "POINTS_FILTER": "[[1, 1], [2, 2]]",
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(
            EXPECTED_PATH, "import_layer_from_profiles_with_points_filter.gml"
        )
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(
                OUTPUT_PATH, "import_layer_from_profiles_with_points_filter.gml"
            )
        save_as_gml(layer, output)

        self.compare_layers(expected, output)

    def test_import_layer_from_profiles_with_mask(self):
        layer = QgsVectorLayer(PROFILES_2014_V1)
        file = PreCourlisFileLine(layer)
        file.add_sedimental_layer("1959", from_layer="zfond", deltaz=-1)

        self.check_algorithm(
            {
                "TARGET_PROFILES": layer,
                "TARGET_LAYER_NAME": "1959",
                "SOURCE_PROFILES": PROFILES_1959,
                "SOURCE_FIELD": "zfond",
                "MASK": MASK,
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(
            EXPECTED_PATH, "import_layer_from_profiles_with_mask.gml"
        )
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(
                OUTPUT_PATH, "import_layer_from_profiles_with_mask.gml"
            )
        save_as_gml(layer, output)

        self.compare_layers(expected, output)

    def test_import_layer_from_profiles_import_source_points(self):
        target_layer = QgsVectorLayer(PROFILES_2014_V1)
        target_file = PreCourlisFileLine(target_layer)
        target_file.add_sedimental_layer(
            "existing_layer", from_layer="zfond", deltaz=-1
        )

        self.check_algorithm(
            {
                "TARGET_PROFILES": target_layer,
                "TARGET_LAYER_NAME": "imported",
                "SOURCE_PROFILES": PROFILES_1959_LAT_STEP_3_5,
                "SOURCE_FIELD": "zfond",
                "IMPORT_SOURCE_POINTS": True,
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(
            EXPECTED_PATH, "import_layer_from_profiles_import_source_points.gml"
        )
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(
                OUTPUT_PATH, "import_layer_from_profiles_import_source_points.gml"
            )
        save_as_gml(target_layer, output)

        self.compare_layers(expected, output)

    def test_import_layer_from_profiles_import_source_points_with_mask(self):
        target_layer = QgsVectorLayer(PROFILES_2014_V1)
        target_file = PreCourlisFileLine(target_layer)
        target_file.add_sedimental_layer(
            "existing_layer", from_layer="zfond", deltaz=-1
        )

        self.check_algorithm(
            {
                "TARGET_PROFILES": target_layer,
                "TARGET_LAYER_NAME": "imported",
                "SOURCE_PROFILES": PROFILES_1959_LAT_STEP_3_5,
                "SOURCE_FIELD": "zfond",
                "IMPORT_SOURCE_POINTS": True,
                "MASK": MASK,
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(
            EXPECTED_PATH,
            "import_layer_from_profiles_import_source_points_with_mask.gml",
        )
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(
                OUTPUT_PATH,
                "import_layer_from_profiles_import_source_points_with_mask.gml",
            )
        save_as_gml(target_layer, output)

        self.compare_layers(expected, output)
