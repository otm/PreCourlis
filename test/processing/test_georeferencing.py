import os

from .. import INPUT_PATH
from . import TestCase


class TestGeoreferencingAlgorithm(TestCase):
    ALGORITHM_ID = "precourlis:georeferencing"
    DEFAULT_PARAMS = {
        "INPUT": os.path.join(INPUT_PATH, "georeferencing", "Garonne_test.geo"),
        "TRACKS": os.path.join(INPUT_PATH, "georeferencing", "test_Garonne_traces.shp"),
    }

    def compare_output(self, key, expected, output):
        if key == "OUTPUT_GEOREF":
            self.compare_files(expected, output)
        else:
            self.compare_layers(expected, output)

    def test_geofererencing_left(self):
        self.check_algorithm(
            {
                "ALIGNMENT": 0,
            },
            {
                "OUTPUT_GEOREF": "georeferencing_left.georef",
                "OUTPUT_LAYER": "georeferencing_left.gml",
            },
        )

    def test_georeferencing_centered(self):
        self.check_algorithm(
            {
                "ALIGNMENT": 1,
            },
            {
                "OUTPUT_GEOREF": "georeferencing_centered.georef",
                "OUTPUT_LAYER": "georeferencing_centered.gml",
            },
        )
