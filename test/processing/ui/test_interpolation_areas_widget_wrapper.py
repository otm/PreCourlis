import pytest
from qgis.core import QgsApplication, QgsProject, QgsVectorLayer
from qgis.PyQt import QtCore
from processing.gui.AlgorithmDialog import AlgorithmDialog

from test import PROFILE_LINES_PATH
from test.processing import TestCase


class TestInterpolationAreasWidgetWrapper(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestInterpolationAreasWidgetWrapper, cls).setUpClass()
        cls.layer = QgsVectorLayer(PROFILE_LINES_PATH, "profiles_lines", "ogr")
        assert cls.layer.isValid()
        QgsProject.instance().addMapLayer(cls.layer)

    @classmethod
    def tearDownClass(cls):
        cls.layer.deleteLater()
        super(TestInterpolationAreasWidgetWrapper, cls).tearDownClass()
        QgsProject.instance().clear()

    @pytest.mark.skip(
        reason=(
            "Create segmentions faults,"
            " probably due to some signal/slot connections with the projet layers changes."
        )
    )
    def test_using_dialog(self):
        # dlg = createAlgorithmDialog("precourlis:interpolate_profiles_per_areas")
        alg = QgsApplication.processingRegistry().createAlgorithmById(
            "precourlis:interpolate_profiles_per_areas"
        )
        dlg = AlgorithmDialog(alg, parent=None)
        wrapper = dlg.mainWidget().wrappers["INTERPOLATION_AREAS"]
        assert wrapper.parent_layer_widget_wrapper is not None
        assert wrapper.parent_layer_widget_wrapper.parameterValue() is not None

        # Close the modal dialog in another loop
        def on_timeout():
            wrapper.interpolation_areas_dialog.accept()  # noqa

        QtCore.QTimer.singleShot(100, on_timeout)

        wrapper.openDialog()

        del wrapper

        dlg.deleteLater()
