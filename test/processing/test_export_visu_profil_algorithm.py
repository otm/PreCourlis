from .. import PROFILE_LINES_PATH
from . import TestCase


class TestExportVisuProfilAlgorithm(TestCase):
    ALGORITHM_ID = "precourlis:export_visu_profil"
    DEFAULT_PARAMS = {"INPUT_PROFILES": PROFILE_LINES_PATH}

    def compare_output(self, key, expected, output):
        pass

    def test_visu_profil(self):
        self.check_algorithm({}, {"OUTPUT": "visu_profil.xlsm"})
