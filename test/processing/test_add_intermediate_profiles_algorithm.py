import os

from qgis.core import QgsVectorLayer, QgsProcessingException

from .. import (
    PROFILE_LINES_PATH,
    DATA_PATH,
    EXPECTED_PATH,
    OVERWRITE_EXPECTED,
    OUTPUT_PATH,
)
from ..utils import save_as_gml
from . import TestCase

AXIS_PATH = os.path.join(DATA_PATH, "input", "axeHydroBief1.shp")


class TestAddIntermediateProfilesAlgorithm(TestCase):
    ALGORITHM_ID = "precourlis:add_intermediate_profiles"
    DEFAULT_PARAMS = {
        "AXIS": AXIS_PATH,
        "CONSTRAINT_LINES": None,
        "CREATE_CONSTRAINT_LINES_ENDS_AT_END_OF_PROFILES": False,
        "LONG_STEP": 100.0,
        "LAT_STEP": 100.0,
        "ATTR_CROSS_SECTION": "sec_id",
    }

    def test_algorithm_success(self):
        layer = QgsVectorLayer(PROFILE_LINES_PATH, "profiles_lines", "ogr")
        layer.select([1, 2])

        self.check_algorithm({"INPUT": layer})

        # Copy temporary layer to output
        expected = os.path.join(EXPECTED_PATH, "add_intermediate_profiles.gml")
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(OUTPUT_PATH, "add_intermediate_profiles.gml")
        save_as_gml(layer, output)

        self.compare_layers(expected, output)

        layer.deleteLater()

    def test_algorithm_failure_because_no_profiles_selected(self):
        layer = QgsVectorLayer(PROFILE_LINES_PATH, "profiles_lines", "ogr")

        with self.assertRaises(
            QgsProcessingException,
            msg="Please select exactly two features (profiles) from the input layer!",
        ):
            self.check_algorithm({"INPUT": layer})

        layer.deleteLater()
