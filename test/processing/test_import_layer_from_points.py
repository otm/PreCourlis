import os

from qgis.core import QgsVectorLayer

from PreCourlis.core.precourlis_file import PreCourlisFileLine

from .. import (
    DATA_PATH,
    EXPECTED_PATH,
    OUTPUT_PATH,
    OVERWRITE_EXPECTED,
)
from ..utils import save_as_gml
from . import TestCase

PROJECT_PATH = os.path.join(DATA_PATH, "input", "Projection_points_traces")
TARGET_PROFILES_PATH = os.path.join(PROJECT_PATH, "profiles.geojson")
SOURCE_POINTS_PATH = os.path.join(PROJECT_PATH, "joined_points.geojson")
MASK = os.path.join(PROJECT_PATH, "mask.geojson")


class TestImportLayerFromPointsAlgorithm(TestCase):

    ALGORITHM_ID = "precourlis:import_layer_from_points"
    DEFAULT_PARAMS = {
        "SOURCE_POINTS": SOURCE_POINTS_PATH,
        "SOURCE_FIELD_SEC_NAME": "sec_name",
        "SOURCE_FIELD_Z": "ELEVATION",
    }

    def test_import_layer_from_points_without_filter(self):
        layer = QgsVectorLayer(TARGET_PROFILES_PATH)
        file = PreCourlisFileLine(layer)
        file.add_sedimental_layer("from_points", from_layer="zfond", deltaz=-1)

        self.check_algorithm(
            {
                "TARGET_PROFILES": layer,
                "TARGET_LAYER_NAME": "from_points",
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(
            EXPECTED_PATH, "import_layer_from_points_without_filter.gml"
        )
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(
                OUTPUT_PATH, "import_layer_from_points_without_filter.gml"
            )
        save_as_gml(layer, output)

        self.compare_layers(expected, output)

    def test_import_layer_from_points_with_points_filter(self):
        layer = QgsVectorLayer(TARGET_PROFILES_PATH)
        file = PreCourlisFileLine(layer)
        file.add_sedimental_layer("from_points", from_layer="zfond", deltaz=-1)

        self.check_algorithm(
            {
                "TARGET_PROFILES": layer,
                "TARGET_LAYER_NAME": "from_points",
                "POINTS_FILTER": "[[3, 12], [3, 13]]",
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(
            EXPECTED_PATH, "import_layer_from_points_with_points_filter.gml"
        )
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(
                OUTPUT_PATH, "import_layer_from_points_with_points_filter.gml"
            )
        save_as_gml(layer, output)

        self.compare_layers(expected, output)

    def test_import_layer_from_points_with_mask(self):
        layer = QgsVectorLayer(TARGET_PROFILES_PATH)
        file = PreCourlisFileLine(layer)
        file.add_sedimental_layer("from_points", from_layer="zfond", deltaz=-1)

        self.check_algorithm(
            {
                "TARGET_PROFILES": layer,
                "TARGET_LAYER_NAME": "from_points",
                "MASK": MASK,
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(EXPECTED_PATH, "import_layer_from_points_with_mask.gml")
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(OUTPUT_PATH, "import_layer_from_points_with_mask.gml")
        save_as_gml(layer, output)

        self.compare_layers(expected, output)
