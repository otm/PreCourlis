import os

from qgis.core import QgsVectorLayer

from .. import (
    PROFILE_LINES_PATH,
    DATA_PATH,
    EXPECTED_PATH,
    OVERWRITE_EXPECTED,
    OUTPUT_PATH,
)
from ..utils import save_as_gml
from . import TestCase
import pytest
from qgis.core import QgsProcessingException

POLYGONS_PATH = os.path.join(DATA_PATH, "input", "topobath_polygons.geojson")


class TestDefineTopoBathAlgorithm(TestCase):
    ALGORITHM_ID = "precourlis:define_topo_bath"
    DEFAULT_PARAMS = {}

    def test_algorithm_success(self):
        layer = QgsVectorLayer(PROFILE_LINES_PATH)

        self.check_algorithm(
            {
                "SECTIONS": layer,
                "POLYGONS": POLYGONS_PATH,
                "TOPO_BATH_EXPRESSION": "topo_bat",
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(EXPECTED_PATH, "profiles_with_updated_topo_bath.gml")
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(OUTPUT_PATH, "profiles_with_updated_topo_bath.gml")
        save_as_gml(layer, output)

        self.compare_layers(output, expected)
        layer.deleteLater()

    def test_algorithm_success_with_different_topo_bath_expression(self):
        layer = QgsVectorLayer(PROFILE_LINES_PATH)

        self.check_algorithm(
            {
                "SECTIONS": layer,
                "POLYGONS": POLYGONS_PATH,
                "TOPO_BATH_EXPRESSION": "topo_bath",
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(EXPECTED_PATH, "profiles_with_updated_topo_bath.gml")
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(OUTPUT_PATH, "profiles_with_updated_topo_bath.gml")
        save_as_gml(layer, output)

        self.compare_layers(output, expected)
        layer.deleteLater()

    def test_algorithm_success_with_T_as_topo_bath_expression(self):
        layer = QgsVectorLayer(PROFILE_LINES_PATH)

        self.check_algorithm(
            {
                "SECTIONS": layer,
                "POLYGONS": POLYGONS_PATH,
                "TOPO_BATH_EXPRESSION": "'T'",
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(
            EXPECTED_PATH, "profiles_with_T_as_topo_bath_expression.gml"
        )
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(
                OUTPUT_PATH, "profiles_with_T_as_topo_bath_expression.gml"
            )
        save_as_gml(layer, output)

        self.compare_layers(output, expected)
        layer.deleteLater()

    def test_algorithm_success_on_selection_with_T_as_topo_bath_expression(self):
        layer = QgsVectorLayer(PROFILE_LINES_PATH)
        layer.select([1])

        self.check_algorithm(
            {
                "SECTIONS": layer,
                "POLYGONS": POLYGONS_PATH,
                "TOPO_BATH_EXPRESSION": "'T'",
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(
            EXPECTED_PATH,
            "profiles_with_T_as_topo_bath_expression_only_on_first_part_of_the_polygon.gml",
        )
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(
                OUTPUT_PATH,
                "profiles_with_T_as_topo_bath_expression_only_on_first_part_of_the_polygon.gml",
            )
        save_as_gml(layer, output)

        self.compare_layers(output, expected)
        layer.deleteLater()

    def test_algorithm_no_sections(self):
        with pytest.raises(QgsProcessingException) as execinfo:
            self.check_algorithm(
                {
                    "POLYGONS": POLYGONS_PATH,
                    "TOPO_BATH_EXPRESSION": "topo_bat",
                }
            )
        assert "Incorrect parameter value for SECTIONS" in str(execinfo.value)

    def test_algorithm_no_polygons(self):
        layer = QgsVectorLayer(PROFILE_LINES_PATH)

        with pytest.raises(QgsProcessingException) as execinfo:
            self.check_algorithm(
                {
                    "SECTIONS": layer,
                    "TOPO_BATH_EXPRESSION": "topo_bat",
                }
            )
        assert (
            "Could not load source layer for POLYGONS: no value specified for parameter"
            in str(execinfo.value)
        )
        layer.deleteLater()

    def test_algorithm_no_topo_bath_expression(self):
        layer = QgsVectorLayer(PROFILE_LINES_PATH)

        with pytest.raises(QgsProcessingException) as execinfo:
            self.check_algorithm(
                {
                    "SECTIONS": layer,
                    "POLYGONS": POLYGONS_PATH,
                }
            )
        assert "Incorrect parameter value for TOPO_BATH_EXPRESSION" in str(
            execinfo.value
        )
        layer.deleteLater()
