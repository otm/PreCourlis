import os

from qgis.core import (
    QgsVectorLayer,
    QgsProcessingException,
)

from PreCourlis.core.interpolation_area import (
    InterpolationArea,
    InterpolationAreaManager,
)

from .. import INPUT_PATH, PROFILE_LINES_PATH, DATA_PATH
from . import TestCase

AXIS_PATH = os.path.join(DATA_PATH, "input", "axeHydroBief1.shp")


class TestInterpolateProfilesPerAreasAlgorithm(TestCase):
    ALGORITHM_ID = "precourlis:interpolate_profiles_per_areas"
    DEFAULT_PARAMS = {
        "SECTIONS": PROFILE_LINES_PATH,
        "AXIS": AXIS_PATH,
        "LATERAL_STEP": 100,
        "ATTR_CROSS_SECTION": "sec_id",
    }

    def test_interpolate_profiles_per_areas(self):
        # Given a profiles layer with interpolation areas as custom properties
        interpolations_areas = [
            InterpolationArea(1, 3, 100.0),
            InterpolationArea(3, 4, 40.0),
            InterpolationArea(4, 6, 80.0),
        ]
        input_profiles = QgsVectorLayer(PROFILE_LINES_PATH)
        InterpolationAreaManager.writeInterpolationAreasOfLayer(
            input_profiles, interpolations_areas
        )

        self.check_algorithm(
            {"SECTIONS": input_profiles},
            {"OUTPUT": "interpolate_profiles_per_areas.gml"},
        )

    def test_interpolate_profiles_per_areas_with_fid(self):
        # Given a profiles layer with interpolation areas as custom properties
        interpolations_areas = [
            InterpolationArea(1, 3, 100.0),
            InterpolationArea(3, 4, 40.0),
            InterpolationArea(4, 6, 80.0),
        ]
        input_profiles = QgsVectorLayer(
            os.path.join(
                INPUT_PATH,
                "profiles_lines_with_fid.gpkg|layername=profiles_lines_with_fid",
            )
        )
        InterpolationAreaManager.writeInterpolationAreasOfLayer(
            input_profiles, interpolations_areas
        )

        self.check_algorithm(
            {"SECTIONS": input_profiles},
            {"OUTPUT": "interpolate_profiles_per_areas_with_fid.gml"},
        )

    def test_interpolate_profiles_per_areas_validation_error(self):
        interpolations_areas = [
            InterpolationArea(5, 3, 100.0),
            InterpolationArea(3, 4, 40.0),
            InterpolationArea(4, 6, 80.0),
        ]
        input_profiles = QgsVectorLayer(PROFILE_LINES_PATH)
        InterpolationAreaManager.writeInterpolationAreasOfLayer(
            input_profiles, interpolations_areas
        )

        with self.assertRaises(
            QgsProcessingException,
            msg="First area's from_profil_id (5) should be the first section id: 1",
        ):
            self.check_algorithm(
                {"SECTIONS": input_profiles},
                {"OUTPUT": "interpolate_profiles_per_areas.gml"},
            )
