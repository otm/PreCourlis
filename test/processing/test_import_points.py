import os

from .. import DATA_PATH
from . import TestCase

PROJECT_PATH = os.path.join(DATA_PATH, "input", "Projection_points_traces")
AXIS_PATH = os.path.join(PROJECT_PATH, "Axe_hydraulique_bathy2022_v2.shp")
TRACKS_PATH = os.path.join(PROJECT_PATH, "profiles.geojson")
SOURCE_POINTS_PATH = os.path.join(PROJECT_PATH, "raw_points.geojson")


class TestImportPointsAlgorithm(TestCase):
    ALGORITHM_ID = "precourlis:import_points"
    DEFAULT_PARAMS = {
        "TRACKS": TRACKS_PATH,
        "TRACKS_FIELD_SEC_ID": "sec_name",
        "AXIS": AXIS_PATH,
        "SOURCE_POINTS": SOURCE_POINTS_PATH,
        "SOURCE_FIELD_SEC_ID": "sec_name",
        "SOURCE_FIELD_Z": "ELEVATION",
        "BUFFER_DISTANCE": 1.0,
    }

    def test_algorithm(self):
        self.check_algorithm({}, {"OUTPUT": "import_points.gml"})
