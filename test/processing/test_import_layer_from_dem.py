import os

from qgis.core import QgsVectorLayer

from .. import (
    DATA_PATH,
    EXPECTED_PATH,
    OVERWRITE_EXPECTED,
    PROFILE_LINES_PATH,
    OUTPUT_PATH,
)
from ..utils import save_as_gml
from . import TestCase

DEM_PATH = os.path.join(DATA_PATH, "input", "cas2Mnt.asc")
MASK_PATH = os.path.join(DATA_PATH, "input", "mask.geojson")


class TestImportLayerFromDemAlgorithm(TestCase):

    ALGORITHM_ID = "precourlis:import_layer_from_dem"
    DEFAULT_PARAMS = {
        "INPUT": PROFILE_LINES_PATH,
        "LAYER_NAME": "Layer1",
        "DEM": DEM_PATH,
        "BAND": 1,
    }

    def test_algorithm(self):
        layer = QgsVectorLayer(PROFILE_LINES_PATH)

        self.check_algorithm(
            {
                "INPUT": layer,
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(EXPECTED_PATH, "import_layer_from_dem.gml")
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(OUTPUT_PATH, "import_layer_from_dem.gml")
        save_as_gml(layer, output)

        self.compare_layers(expected, output)

    def test_import_layer_from_dem_with_default_elevation(self):
        layer = QgsVectorLayer(PROFILE_LINES_PATH)

        self.check_algorithm(
            {
                "INPUT": layer,
                "DEFAULT_ELEVATION": 100.0,
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(
            EXPECTED_PATH, "import_layer_from_dem_with_default_elevation.gml"
        )
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(
                OUTPUT_PATH, "import_layer_from_dem_with_default_elevation.gml"
            )
        save_as_gml(layer, output)

        self.compare_layers(expected, output)

    def test_import_layer_from_dem_with_points_filter(self):
        layer = QgsVectorLayer(PROFILE_LINES_PATH)

        self.check_algorithm(
            {
                "INPUT": layer,
                "POINTS_FILTER": "[[1, 1], [2, 2]]",
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(
            EXPECTED_PATH, "import_layer_from_dem_with_points_filter.gml"
        )
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(
                OUTPUT_PATH, "import_layer_from_dem_with_points_filter.gml"
            )
        save_as_gml(layer, output)

        self.compare_layers(expected, output)

    def test_import_layer_from_dem_with_mask(self):
        layer = QgsVectorLayer(PROFILE_LINES_PATH)

        self.check_algorithm(
            {
                "INPUT": layer,
                "MASK": MASK_PATH,
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(EXPECTED_PATH, "import_layer_from_dem_with_mask.gml")
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(OUTPUT_PATH, "import_layer_from_dem_with_mask.gml")
        save_as_gml(layer, output)

        self.compare_layers(expected, output)

    def test_import_layer_from_dem_with_points_filter_and_mask(self):
        layer = QgsVectorLayer(PROFILE_LINES_PATH)

        self.check_algorithm(
            {
                "INPUT": layer,
                "MASK": MASK_PATH,
                "POINTS_FILTER": "[[1, 3], [1, 4], [2, 4]]",
            }
        )

        # Copy temporary layer to output
        expected = os.path.join(
            EXPECTED_PATH, "import_layer_from_dem_with_points_filter_and_mask.gml"
        )
        if OVERWRITE_EXPECTED:
            output = expected
        else:
            output = os.path.join(
                OUTPUT_PATH, "import_layer_from_dem_with_points_filter_and_mask.gml"
            )
        save_as_gml(layer, output)

        self.compare_layers(expected, output)
