from qgis.core import QgsProcessingException

from .. import PROFILE_LINES_PATH
from . import TestCase

import filecmp
import os


class TestExportMobiliAlgorithm(TestCase):
    ALGORITHM_ID = "precourlis:export_mobili"
    DEFAULT_PARAMS = {"INPUT_PROFILES": PROFILE_LINES_PATH}

    def compare_output(self, key, output, expected):
        assert os.path.isdir(expected)
        assert os.path.isdir(output)

        # compare two directories
        dcmp = filecmp.dircmp(expected, output)
        assert len(dcmp.diff_files) == 0
        assert len(dcmp.left_only) == 0
        assert len(dcmp.right_only) == 0
        assert len(dcmp.funny_files) == 0

    def test_export_mobili(self):
        self.check_algorithm({}, {"OUTPUT_FOLDER": "export_mobili"})

    def test_reach_name_validation_for_mobili_export(self):
        with self.assertRaises(
            QgsProcessingException, msg="Reach name cannot contain spaces"
        ):
            self.check_algorithm(
                {
                    "REACH_NAME": "A name with spaces",
                    "OUTPUT_FOLDER": "reach_name_validation_for_mobili_export",
                }
            )
