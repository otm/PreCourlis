import unittest

from qgis.core import QgsVectorLayer
from qgis.PyQt import QtCore
from qgis.PyQt.QtCore import Qt

from PreCourlis.widgets.interpolation_areas_table_model import (
    InterpolationAreasTableModel,
    InterpolationArea,
)

from .. import PROFILE_LINES_PATH


class TestInterpolationAreasTableModel(unittest.TestCase):

    INTERPOLATION_AREAS = [
        InterpolationArea(1, 5, 100.0),
        InterpolationArea(5, 6, 80.0),
    ]

    INTERPOLATION_AREAS_STR = (
        "["
        '{"from_profil_id": 1,'
        '"to_profil_id": 5,'
        '"longitudinal_step": 100.0},'
        '{"from_profil_id": 5,'
        '"to_profil_id": 6,'
        '"longitudinal_step": 80.0}]'
    )

    def defaultInterpolationAreas(self):
        """
        Return a deep copy self.INTERPOLATION_AREAS so that we never alter original ones.
        """
        return [
            InterpolationArea.fromDict(ia.toDict()) for ia in self.INTERPOLATION_AREAS
        ]

    def test_givenTableModel_whenCreated_thenNoData(self):
        tableModel = InterpolationAreasTableModel()

        expected = tableModel.interpolationAreas()
        assert expected == []

    def test_givenTableModel_whenSetInterpolationAreas_thenGetBackInterpolationAreas(
        self,
    ):
        tableModel = InterpolationAreasTableModel()

        tableModel.setInterpolationAreas(self.INTERPOLATION_AREAS)

        expected = tableModel.interpolationAreas()
        assert expected == self.INTERPOLATION_AREAS

    def test_rowCount(self):
        tableModel = InterpolationAreasTableModel()
        assert tableModel.rowCount() == 0

        tableModel.setInterpolationAreas(self.defaultInterpolationAreas())
        assert tableModel.rowCount() == 2

    def test_columnCount(self):
        tableModel = InterpolationAreasTableModel()
        assert tableModel.columnCount() == 3

    def test_headerData(self):
        tableModel = InterpolationAreasTableModel(None)

        assert tableModel.headerData(0, QtCore.Qt.Horizontal) == "from_profil_id"
        assert tableModel.headerData(1, QtCore.Qt.Horizontal) == "to_profil_id"
        assert tableModel.headerData(2, QtCore.Qt.Horizontal) == "longitudinal_step"
        assert tableModel.headerData(0, QtCore.Qt.Vertical) == "0"

    def test_data(self):
        layer = QgsVectorLayer(PROFILE_LINES_PATH, "profiles_lines", "ogr")

        tableModel = InterpolationAreasTableModel()
        tableModel.setLayer(layer)
        tableModel.setInterpolationAreas(self.defaultInterpolationAreas())

        # DisplayRole
        assert tableModel.data(tableModel.index(0, 0), Qt.DisplayRole) == "1: P1 (0.0)"
        assert (
            tableModel.data(tableModel.index(0, 1), Qt.DisplayRole)
            == "5: P5 (2826.747)"
        )
        assert tableModel.data(tableModel.index(0, 2), Qt.DisplayRole) == "100.0"

        # EditRole
        assert (
            tableModel.data(tableModel.index(0, 0), Qt.EditRole)
            == self.INTERPOLATION_AREAS[0].from_profil_id
        )
        assert (
            tableModel.data(tableModel.index(0, 1), Qt.EditRole)
            == self.INTERPOLATION_AREAS[0].to_profil_id
        )
        assert (
            tableModel.data(tableModel.index(0, 2), Qt.EditRole)
            == self.INTERPOLATION_AREAS[0].longitudinal_step
        )

    def test_setData(self):
        tableModel = InterpolationAreasTableModel(None)
        tableModel.setInterpolationAreas(self.INTERPOLATION_AREAS)

        assert tableModel.setData(tableModel.index(0, 0), 1, QtCore.Qt.EditRole)
        assert tableModel.interpolationAreas()[0].from_profil_id == 1

        assert tableModel.setData(tableModel.index(0, 1), 2, QtCore.Qt.EditRole)
        assert tableModel.interpolationAreas()[0].to_profil_id == 2

        assert tableModel.setData(tableModel.index(0, 2), 100.0, QtCore.Qt.EditRole)
        assert tableModel.interpolationAreas()[0].longitudinal_step == 100.0

    def test_flags(self):
        tableModel = InterpolationAreasTableModel(None)

        assert (
            tableModel.flags(tableModel.index(0, 0))
            == QtCore.Qt.ItemIsEnabled
            | QtCore.Qt.ItemIsEditable
            | QtCore.Qt.ItemIsSelectable
        )

    def test_insertRows(self):
        tableModel = InterpolationAreasTableModel(None)
        tableModel.setInterpolationAreas(self.defaultInterpolationAreas())

        assert tableModel.insertRows(1, 1)
        assert len(tableModel.interpolationAreas()) == 3

    def test_removeRows(self):
        tableModel = InterpolationAreasTableModel(None)
        tableModel.setInterpolationAreas(self.defaultInterpolationAreas())

        assert tableModel.removeRows(-1, 1)
        assert len(tableModel.interpolationAreas()) == 1

    def test_returns_last_sec_id_when_layer_not_none_and_contains_features(self):
        # Create an instance of InterpolationAreasTableModel and set the layer
        layer = QgsVectorLayer(PROFILE_LINES_PATH, "profiles_lines", "ogr")

        tableModel = InterpolationAreasTableModel()
        tableModel.setLayer(layer)

        # Call the getLastProfilSecId method
        result = tableModel.getLastProfilSecId()

        # Assert the result is the last sec_id value
        assert result == 6

    def test_returns_first_sec_id_when_layer_not_none_and_contains_features(self):
        # Create an instance of InterpolationAreasTableModel and set the layer
        layer = QgsVectorLayer(PROFILE_LINES_PATH, "profiles_lines", "ogr")

        tableModel = InterpolationAreasTableModel()
        tableModel.setLayer(layer)

        # Call the getLastProfilSecId method
        result = tableModel.getFirstProfilSecId()

        # Assert the result is the last sec_id value
        assert result == 1

    # Returns 0 when the 'QgsVectorLayer' object is None.
    def test_getLastProfilSecId_returns_0_when_layer_is_none(self):
        # Create an instance of InterpolationAreasTableModel without setting the layer
        tableModel = InterpolationAreasTableModel()

        # Call the getLastProfilSecId method
        result = tableModel.getLastProfilSecId()

        # Assert the result is 0
        assert result == 0

        # Returns 0 when the 'QgsVectorLayer' object is None.

    def test_getFirstProfilSecId_returns_0_when_layer_is_none(self):
        # Create an instance of InterpolationAreasTableModel without setting the layer
        tableModel = InterpolationAreasTableModel()

        # Call the getLastProfilSecId method
        result = tableModel.getFirstProfilSecId()

        # Assert the result is 0
        assert result == 0
