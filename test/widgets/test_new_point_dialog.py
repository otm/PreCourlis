import unittest

from qgis.core import QgsFeatureRequest, QgsVectorLayer
import numpy as np

from PreCourlis.core.precourlis_file import PreCourlisFileLine
from PreCourlis.widgets.new_point_dialog import NewPointDialog

from .. import PROFILE_LINES_PATH


class TestNewPointDialog(unittest.TestCase):
    """Test dialog works."""

    @classmethod
    def setUpClass(cls):
        super(TestNewPointDialog, cls).setUpClass()
        cls.layer = QgsVectorLayer(PROFILE_LINES_PATH, "profiles_lines", "ogr")
        assert cls.layer.isValid()

    @classmethod
    def tearDownClass(cls):
        cls.layer.deleteLater()
        super(TestNewPointDialog, cls).tearDownClass()

    def section(self):
        request = QgsFeatureRequest()
        request.setLimit(1)
        feature = next(self.layer.getFeatures(request))
        return PreCourlisFileLine(self.layer).section_from_feature(feature)

    def test_init(self):
        section = self.section()

        NewPointDialog(section, 1, None)

    def test_update_infos_intermediate_point(self):
        section = self.section()

        dlg = NewPointDialog(section, 1, None)

        assert dlg.idInfoLabel.text() == "2 - 3"
        assert dlg.idSpinBox.value() == 3

        assert dlg.absLatInfoLabel.text() == "100.0 - 200.0"
        assert dlg.absLatSpinBox.minimum() == 100.0
        assert dlg.absLatSpinBox.maximum() == 200.0

        assert dlg.xInfoLabel.text() == "427847.22 - 427850.69"
        assert dlg.xSpinBox.minimum() == 427847.22
        assert dlg.xSpinBox.maximum() == 427850.69

        assert dlg.yInfoLabel.text() == "244748.38 - 244848.32"
        assert dlg.ySpinBox.minimum() == 244748.38
        assert dlg.ySpinBox.maximum() == 244848.32

        assert dlg.topoBathInfoLabel.text() == "B - B"

        assert dlg.zInfoLabel.text() == "22.004 - 21.561"
        assert dlg.zSpinBox.minimum() == 0.0
        assert dlg.zSpinBox.maximum() == 10000000.0

        assert dlg.layers_info_labels[0].text() == "21.004 - 20.561"
        assert dlg.layers_spinboxes[0].minimum() == 0.0
        assert dlg.layers_spinboxes[0].maximum() == 10000000.0

    def test_update_infos_last_point(self):
        section = self.section()
        dlg = NewPointDialog(section, 8, None)

        assert dlg.idInfoLabel.text() == "9 - "
        assert dlg.idSpinBox.value() == 10

        assert dlg.absLatInfoLabel.text() == "776.57 - "
        assert dlg.absLatSpinBox.minimum() == 776.57
        assert dlg.absLatSpinBox.maximum() == 10000000.0

        assert dlg.xInfoLabel.text() == "427870.69 - "
        assert dlg.xSpinBox.minimum() == 0.0
        assert dlg.xSpinBox.maximum() == 10000000.0

        assert dlg.yInfoLabel.text() == "245424.54 - "
        assert dlg.ySpinBox.minimum() == 0
        assert dlg.ySpinBox.maximum() == 10000000.0

        assert dlg.topoBathInfoLabel.text() == "B - "

        assert dlg.zInfoLabel.text() == "12.896 - "
        assert dlg.zSpinBox.minimum() == 0.0
        assert dlg.zSpinBox.maximum() == 10000000.0

        assert dlg.layers_info_labels[0].text() == "11.896 - "
        assert dlg.layers_spinboxes[0].minimum() == 0.0
        assert dlg.layers_spinboxes[0].maximum() == 10000000.0

    def test_refresh_abs_lat(self):
        section = self.section()
        dlg = NewPointDialog(section, 1, None)

        dlg.refresh_abs_lat()

        assert dlg.abs_lat() == 150.0

    def test_refresh_x_intermediate_point(self):
        section = self.section()
        dlg = NewPointDialog(section, 1, None)

        assert dlg.x() == 427848.95

        dlg.set_abs_lat(125)
        dlg.refresh_x()

        assert dlg.x() == 427848.08

    def test_refresh_x_last_point(self):
        section = self.section()
        dlg = NewPointDialog(section, 8, None)

        assert dlg.abs_lat() == 776.57
        assert dlg.x() == 427870.69

        dlg.set_abs_lat(800.0)
        dlg.refresh_x()

        assert dlg.x() == 427871.51

    def test_refresh_y(self):
        section = self.section()
        dlg = NewPointDialog(section, 1, None)

        assert dlg.y() == 244798.35

        dlg.set_abs_lat(125)
        dlg.refresh_y()

        assert dlg.y() == 244773.37

    def test_refresh_topo_bath_intermediate_point(self):
        section = self.section()
        dlg = NewPointDialog(section, 1, None)

        # B on each side
        section.topo_bath[1] = "B"
        section.topo_bath[2] = "B"
        dlg.refresh_topo_bath()
        assert dlg.topo_bath() == "B"

        # T on each side
        section.topo_bath[1] = "T"
        section.topo_bath[2] = "T"
        dlg.refresh_topo_bath()
        assert dlg.topo_bath() == "T"

        # T on one side and B on other
        section.topo_bath[1] = "T"
        section.topo_bath[2] = "B"
        dlg.refresh_topo_bath()
        assert dlg.topo_bath() is None

    def test_refresh_topo_bath_last_point(self):
        section = self.section()
        dlg = NewPointDialog(section, 8, None)

        # B on left side and nothing on right
        section.topo_bath[8] = "B"
        dlg.refresh_topo_bath()
        assert dlg.topo_bath() is None

        # T on left side and nothing on right
        section.topo_bath[8] = "T"
        dlg.refresh_topo_bath()
        assert dlg.topo_bath() == "T"

    def test_validation_error(self):
        section = self.section()
        dlg = NewPointDialog(section, 1, None)
        dlg.set_topo_bath(None)
        dlg.accept()

        assert dlg.message_bar.items()[0].text() == "Field topo_bat is required"

    def test_accept(self):
        section = self.section()
        dlg = NewPointDialog(section, 1, None)

        dlg.accept()

        assert section.distances[2] == 150.0
        assert section.x[2] == 427848.95
        assert section.y[2] == 244798.35
        assert section.topo_bath[2] == "B"
        assert section.z[2] == 21.783
        assert section.layers_elev[0][2] == 20.783

    def test_accept_with_no_layers(self):
        section = self.section()
        section.nlayers = 0
        section.layer_names = []
        section.layers_elev = np.array([])

        dlg = NewPointDialog(section, 1, None)

        dlg.accept()

        assert section.distances[2] == 150.0
        assert section.x[2] == 427848.95
        assert section.y[2] == 244798.35
        assert section.topo_bath[2] == "B"
        assert section.z[2] == 21.783
        assert len(section.layers_elev) == 0
