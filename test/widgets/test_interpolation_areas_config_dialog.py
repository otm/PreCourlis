import unittest

from qgis.core import (
    QgsVectorLayer,
    QgsProject,
)

from PreCourlis.core.interpolation_area import (
    InterpolationArea,
    InterpolationAreaManager,
)

from PreCourlis.widgets.interpolation_areas_config_dialog import (
    InterpolationAreasConfigDialog,
    ProfilIdComboBoxDelegate,
)

from .. import PROFILE_LINES_PATH, iface


class TestInterpolationAreasConfigDialog(unittest.TestCase):

    INTERPOLATION_AREAS = [
        InterpolationArea(1, 5, 100.0),
        InterpolationArea(5, 6, 80.0),
    ]

    def setUp(self):
        self.layer = QgsVectorLayer(PROFILE_LINES_PATH, "profiles_lines", "ogr")
        assert self.layer.isValid()
        QgsProject.instance().addMapLayer(self.layer)

    def tearDown(self):
        QgsProject.instance().clear()
        iface.activeLayer.return_value = None

    def test_addRowButtonClicked(self):
        configDialog = InterpolationAreasConfigDialog()
        assert len(configDialog._interpolationAreasTableModel._interpolationAreas) == 0
        configDialog.addRowButtonClicked()
        assert len(configDialog._interpolationAreasTableModel._interpolationAreas) == 1

        # Needed otherwise SaveConfigDialog is shown and block everything
        configDialog.profilesLayerChanged(None)

    def test_removeRowButtonClicked(self):
        configDialog = InterpolationAreasConfigDialog()
        configDialog.addRowButtonClicked()
        assert len(configDialog._interpolationAreasTableModel._interpolationAreas) == 1

        # Select a row and delete it
        configDialog.interpolationAreasTableView.selectRow(0)
        configDialog.removeRowButtonClicked()
        assert len(configDialog._interpolationAreasTableModel._interpolationAreas) == 0

        # Needed otherwise SaveConfigDialog is shown and block everything
        configDialog.profilesLayerChanged(None)

    def test_givenAnEmptyActiveLayer_whenCreateConfigDialog_thenLoadNoInterpolationsAreas(
        self,
    ):
        iface.activeLayer.return_value = self.layer

        configDialog = InterpolationAreasConfigDialog()

        assert configDialog._interpolationAreasTableModel.rowCount() == 0
        assert configDialog._interpolationAreasTableModel._interpolationAreas == []

        # Needed otherwise SaveConfigDialog is shown and block everything
        configDialog.profilesLayerChanged(None)

    def test_givenFilledActiveLayer_whenCreateConfigDialog_thenLoadInterpolationsAreas(
        self,
    ):
        iface.activeLayer.return_value = self.layer
        InterpolationAreaManager.writeInterpolationAreasOfLayer(
            self.layer, self.INTERPOLATION_AREAS
        )

        configDialog = InterpolationAreasConfigDialog()

        assert configDialog._interpolationAreasTableModel.rowCount() == 2
        ias = configDialog._interpolationAreasTableModel.interpolationAreas()

        assert ias[0].from_profil_id == self.INTERPOLATION_AREAS[0].from_profil_id
        assert ias[0].to_profil_id == self.INTERPOLATION_AREAS[0].to_profil_id
        assert ias[0].longitudinal_step == self.INTERPOLATION_AREAS[0].longitudinal_step

        assert ias[1].from_profil_id == self.INTERPOLATION_AREAS[1].from_profil_id
        assert ias[1].to_profil_id == self.INTERPOLATION_AREAS[1].to_profil_id
        assert ias[1].longitudinal_step == self.INTERPOLATION_AREAS[1].longitudinal_step

        # Needed otherwise SaveConfigDialog is shown and block everything
        configDialog.profilesLayerChanged(None)

    def test_save(self):
        iface.activeLayer.return_value = self.layer
        InterpolationAreaManager.writeInterpolationAreasOfLayer(
            self.layer, self.INTERPOLATION_AREAS
        )
        configDialog = InterpolationAreasConfigDialog()
        assert len(configDialog._interpolationAreasTableModel.interpolationAreas()) == 2
        configDialog.save()

    def test_reset(self):
        iface.activeLayer.return_value = self.layer
        InterpolationAreaManager.writeInterpolationAreasOfLayer(
            self.layer, self.INTERPOLATION_AREAS
        )
        configDialog = InterpolationAreasConfigDialog()
        assert len(configDialog._interpolationAreasTableModel.interpolationAreas()) == 2

        configDialog._interpolationAreasTableModel.setInterpolationAreas([])
        configDialog.reset()

        assert len(configDialog._interpolationAreasTableModel.interpolationAreas()) == 2


class TestProfilIdComboBoxDelegate:
    AVAILABLE_PROFILES = [
        (0, "P1", 500.001),
        (1, "P2", 1000.0),
    ]

    def test_createEditor(self):
        delegate = ProfilIdComboBoxDelegate(None)
        delegate.setAvailableProfiles(self.AVAILABLE_PROFILES)
        cb = delegate.createEditor(None, None, None)
        assert cb.itemData(0) == 0
        assert cb.itemText(0) == "0: P1 (500.001)"
