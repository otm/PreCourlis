# PreCourlis QGIS plugin

QGIS 3 plugin that is meant to model the bed of rivers and export data (sediment layered 1D cross-sections) for Courlis, which is the sediment transport module for MASCARET (the 1D free surface flow code of the Open [TELEMAC-MASCARET platform](http://www.opentelemac.org/)).

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](LICENSE.md)
![Continuous integration](https://github.com/msecher/PreCourlis/workflows/Continuous%20integration/badge.svg)

Check the [wiki](https://gitlab.pam-retd.fr/otm/PreCourlis/-/wikis/home) for more informations.
