from qgis.core import (
    QgsProcessing,
    QgsProcessingParameterFeatureSource,
    QgsProcessingParameterFeatureSink,
    QgsProcessingOutputLayerDefinition,
)

import processing
from PreCourlis.processing.precourlis_algorithm import PreCourlisAlgorithm


class RepairProfilesAlgorithm(PreCourlisAlgorithm):
    INPUT = "INPUT"
    OUTPUT = "OUTPUT"

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT,
                self.tr("Input"),
                types=[QgsProcessing.TypeVectorLine],
                defaultValue=None,
            )
        )

        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT,
                self.tr("Output"),
                type=QgsProcessing.TypeVectorLine,
                createByDefault=True,
                defaultValue=None,
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        output = QgsProcessingOutputLayerDefinition(parameters[self.OUTPUT])
        output.destinationName = self.tr("Repaired profiles")

        TOPO_BAT_EXPR = (
            "'B' || replace(rpad('', num_points($geometry) - 1, ','), ',', ',B')"
        )

        outputs = {}

        alg_params = {
            "INPUT": parameters[self.INPUT],
            "FIELDS_MAPPING": [
                {
                    "expression": '"sec_id"',
                    "length": 0,
                    "name": "sec_id",
                    "precision": 0,
                    "type": 2,
                },
                {
                    "expression": '"sec_name"',
                    "length": 0,
                    "name": "sec_name",
                    "precision": 0,
                    "type": 10,
                },
                {
                    "expression": '"abs_long"',
                    "length": 0,
                    "name": "abs_long",
                    "precision": 0,
                    "type": 6,
                },
                {
                    "expression": '"axis_x"',
                    "length": 0,
                    "name": "axis_x",
                    "precision": 0,
                    "type": 6,
                },
                {
                    "expression": '"axis_y"',
                    "length": 0,
                    "name": "axis_y",
                    "precision": 0,
                    "type": 6,
                },
                {
                    "expression": "''",
                    "length": 0,
                    "name": "layers",
                    "precision": 0,
                    "type": 10,
                },
                {
                    "expression": '"p_id"',
                    "length": 0,
                    "name": "p_id",
                    "precision": 0,
                    "type": 10,
                },
                {
                    "expression": TOPO_BAT_EXPR,
                    "length": 0,
                    "name": "topo_bat",
                    "precision": 0,
                    "type": 10,
                },
                {
                    "expression": '"abs_lat"',
                    "length": 0,
                    "name": "abs_lat",
                    "precision": 0,
                    "type": 10,
                },
                {
                    "expression": '"zfond"',
                    "length": 0,
                    "name": "zfond",
                    "precision": 0,
                    "type": 10,
                },
            ],
            "OUTPUT": output,
            # "OUTPUT": parameters[self.OUTPUT],
        }
        outputs["refactorfields"] = processing.run(
            "native:refactorfields",
            alg_params,
            context=context,
            feedback=feedback,
            is_child_algorithm=True,
        )

        return {self.OUTPUT: outputs["refactorfields"]["OUTPUT"]}

    def name(self):
        return "repair_profiles"

    def displayName(self):
        return self.tr("Repair profiles layer")

    def shortHelpString(self):
        return self.tr(
            "This algorithm repair a profiles layer by adding missing topo_bat field."
        )

    def group(self):
        return self.tr("Profiles")

    def groupId(self):
        return "Profiles"

    def createInstance(self):
        return RepairProfilesAlgorithm()
