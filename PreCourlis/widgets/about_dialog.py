# -*- coding: utf-8 -*-

from pkg_resources import resource_filename

from qgis.PyQt import uic, QtGui, QtWidgets

from PreCourlis import metadata

# This loads your .ui file so that PyQt can populate your plugin with the elements from Qt Designer
FORM_CLASS, _ = uic.loadUiType(resource_filename("PreCourlis", "ui/about_dialog.ui"))


class AboutDialog(QtWidgets.QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)

        self.titleLabel.setText(f"PreCourlis v{metadata['version']}")
        icon = QtGui.QImage(resource_filename("PreCourlis", "resources/icon.png"))
        self.iconLabel.setPixmap(QtGui.QPixmap.fromImage(icon))
        self.aboutLabel.setText(metadata["about"])
