<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../ui/about_dialog.ui" line="45"/>
        <source>PreCourlis vX.Y.Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/about_dialog.ui" line="28"/>
        <source>icon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/about_dialog.ui" line="73"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/about_dialog.ui" line="14"/>
        <source>About PreCourlis</source>
        <translation>À propos de PreCoulis</translation>
    </message>
</context>
<context>
    <name>AddIntermediateProfilesAlgorithm</name>
    <message>
        <location filename="../processing/add_intermediate_profiles_algorithm.py" line="33"/>
        <source>Input</source>
        <translation>Entrée</translation>
    </message>
    <message>
        <location filename="../processing/add_intermediate_profiles_algorithm.py" line="41"/>
        <source>Axis</source>
        <translation>Axe</translation>
    </message>
    <message>
        <location filename="../processing/add_intermediate_profiles_algorithm.py" line="58"/>
        <source>Create constraint lines at profiles ends (avoid errors when some constraint lines doesn&apos;t cross all profiles)</source>
        <translation>Créer des lignes de contraintes aux extrémites des profils (évite des erreurs lorsque certaines lignes de contraintes ne croisent pas tous les profils)</translation>
    </message>
    <message>
        <location filename="../processing/add_intermediate_profiles_algorithm.py" line="69"/>
        <source>Longitudinal space step (in m)</source>
        <translation>Pas longitudinal (en mètres)</translation>
    </message>
    <message>
        <location filename="../processing/add_intermediate_profiles_algorithm.py" line="77"/>
        <source>Lateral space step (in m)</source>
        <translation>Pas latéral (en mètres)</translation>
    </message>
    <message>
        <location filename="../processing/add_intermediate_profiles_algorithm.py" line="85"/>
        <source>Attribute to identify cross-sections</source>
        <translation>Attribut identifiant des sections</translation>
    </message>
    <message>
        <location filename="../processing/add_intermediate_profiles_algorithm.py" line="101"/>
        <source>Please select exactly two features (profiles) from the input layer!</source>
        <translation>Sélectionnez exactement deux entités (profils) depuis la couche d&apos;entrée !</translation>
    </message>
    <message>
        <location filename="../processing/add_intermediate_profiles_algorithm.py" line="108"/>
        <source>Extracting selected features (profiles) from layer:</source>
        <translation>Extraction des entités sélectionnées (profils) de la couche :</translation>
    </message>
    <message>
        <location filename="../processing/add_intermediate_profiles_algorithm.py" line="125"/>
        <source>Lines interpolation:</source>
        <translation>Interpolation des lignes :</translation>
    </message>
    <message>
        <location filename="../processing/add_intermediate_profiles_algorithm.py" line="197"/>
        <source>Add intermediate profiles</source>
        <translation>Ajouter les profiles intermédiaires</translation>
    </message>
    <message>
        <location filename="../processing/add_intermediate_profiles_algorithm.py" line="200"/>
        <source>This algorithm add intermediate profiles interpolated between two selected profiles from the input layer.

It writes the result directly on the input layer.

Don&apos;t forget to select two features (profiles) from your input profiles layer!</source>
        <translation>Cet algortihme ajoute des profils intermédiaires calculés par interpolation entre les deux profils sélectionnés dans la couche en entrée.

Il écrit directement le résultat dans la couche en entrée.

N&apos;oubliez pas de sélectionner deux entités (profils) depuis votre couche de profils en entrée !</translation>
    </message>
    <message>
        <location filename="../processing/add_intermediate_profiles_algorithm.py" line="210"/>
        <source>Profiles</source>
        <translation>Profils</translation>
    </message>
    <message>
        <location filename="../processing/add_intermediate_profiles_algorithm.py" line="49"/>
        <source>Contraint lines</source>
        <translation>Lignes de contraintes</translation>
    </message>
    <message>
        <location filename="../processing/add_intermediate_profiles_algorithm.py" line="151"/>
        <source>Adding interpolated lines to layer:</source>
        <translation>Ajout des lignes interpolés à la couche :</translation>
    </message>
    <message>
        <location filename="../processing/add_intermediate_profiles_algorithm.py" line="155"/>
        <source>Added intermediate profiles</source>
        <translation>Ajout de profils intermédaires</translation>
    </message>
</context>
<context>
    <name>AddPointDialog</name>
    <message>
        <location filename="../ui/new_point_dialog.ui" line="14"/>
        <source>Add new point</source>
        <translation>Ajouter un nouveau point</translation>
    </message>
    <message>
        <location filename="../ui/new_point_dialog.ui" line="152"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../ui/new_point_dialog.ui" line="35"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="../ui/new_point_dialog.ui" line="166"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/new_point_dialog.ui" line="96"/>
        <source>Topo / Bathy</source>
        <translation>Topo / Bathy</translation>
    </message>
    <message>
        <location filename="../ui/new_point_dialog.ui" line="103"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../ui/new_point_dialog.ui" line="117"/>
        <source>Z fond</source>
        <translation>Z fond</translation>
    </message>
    <message>
        <location filename="../ui/new_point_dialog.ui" line="145"/>
        <source>Lateral abscissa</source>
        <translation>Abscisse latérale</translation>
    </message>
    <message>
        <location filename="../ui/new_point_dialog.ui" line="159"/>
        <source>X</source>
        <translation>X</translation>
    </message>
</context>
<context>
    <name>DefineTopoBathAlgorithm</name>
    <message>
        <location filename="../processing/define_topo_bath_algorithm.py" line="19"/>
        <source>Sections</source>
        <translation>Sections</translation>
    </message>
    <message>
        <location filename="../processing/define_topo_bath_algorithm.py" line="26"/>
        <source>Polygons</source>
        <translation>Polygones</translation>
    </message>
    <message>
        <location filename="../processing/define_topo_bath_algorithm.py" line="33"/>
        <source>Topography / bathymetry expression</source>
        <translation>Expression topographie / bathymétrie</translation>
    </message>
    <message>
        <location filename="../processing/define_topo_bath_algorithm.py" line="56"/>
        <source>Converting sections lines to points:</source>
        <translation>Convertion des lignes des sections en points :</translation>
    </message>
    <message>
        <location filename="../processing/define_topo_bath_algorithm.py" line="71"/>
        <source>Deleting old topo_bat attribute:</source>
        <translation>Suppression de l&apos;ancien attribut topo_bat :</translation>
    </message>
    <message>
        <location filename="../processing/define_topo_bath_algorithm.py" line="138"/>
        <source>Fields refactoring:</source>
        <translation>Modification des champs :</translation>
    </message>
    <message>
        <location filename="../processing/define_topo_bath_algorithm.py" line="250"/>
        <source>Converting points back to lines:</source>
        <translation>Re-convertion des points des sections en lignes :</translation>
    </message>
    <message>
        <location filename="../processing/define_topo_bath_algorithm.py" line="268"/>
        <source>Applying changes to sections:</source>
        <translation>Appliquation des modifications aux sections :</translation>
    </message>
    <message>
        <location filename="../processing/define_topo_bath_algorithm.py" line="304"/>
        <source>Define topography / bathymetry from a polygon</source>
        <translation>Définir la topographie/bathymétrie à partir d&apos;un polygone</translation>
    </message>
    <message>
        <location filename="../processing/define_topo_bath_algorithm.py" line="307"/>
        <source>Profiles</source>
        <translation>Profils</translation>
    </message>
    <message>
        <location filename="../processing/define_topo_bath_algorithm.py" line="316"/>
        <source>This algorithm update the topo_bath attribute of the input sections layer according to their location inside the provided polygons source, or selection.
Each polygons should either have a Topography / Bathymetry expression set as an attribute or we should give to the algorithm a default value (&apos;T&apos; or &apos;B&apos;).

It writes the result directly on the input sections layer.</source>
        <translation>Cet algorithme met à jour l&apos;attribut topo_bath de la couche de sections en entrée en fonction de leur emplacement à l&apos;intérieur de la source ou de la sélection des polygones fournis.
Chaque polygone doit soit avoir une expression topographique/bathymétrique définie comme attribut, soit donner à l&apos;algorithme une valeur par défaut (« T » ou « B »).

Il écrit le résultat directement sur la couche des sections d&apos;entrée.</translation>
    </message>
    <message>
        <location filename="../processing/define_topo_bath_algorithm.py" line="90"/>
        <source>Evaluating expression on polygons:</source>
        <translation>Evaluation de l&apos;expression sur les polygones :</translation>
    </message>
    <message>
        <location filename="../processing/define_topo_bath_algorithm.py" line="113"/>
        <source>Joining topo_bath attribute:</source>
        <translation>Jointure de l&apos;attribut topo_bat :</translation>
    </message>
</context>
<context>
    <name>ExportCourlisAlgorithm</name>
    <message>
        <location filename="../processing/export_courlis_algorithm.py" line="26"/>
        <source>Input</source>
        <translation>Entrée</translation>
    </message>
    <message>
        <location filename="../processing/export_courlis_algorithm.py" line="35"/>
        <source>Reach name (default to input layer name)</source>
        <translation>Nom du bief (nom de la couche d&apos;entrée par défaut)</translation>
    </message>
    <message>
        <location filename="../processing/export_courlis_algorithm.py" line="43"/>
        <source>Output file</source>
        <translation>Ficher de sortie</translation>
    </message>
    <message>
        <location filename="../processing/export_courlis_algorithm.py" line="64"/>
        <source>Reach name cannot contain spaces</source>
        <translation>Le nom du bief ne peut pas contenir d&apos;espaces</translation>
    </message>
    <message>
        <location filename="../processing/export_courlis_algorithm.py" line="96"/>
        <source>Export Courlis</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/export_courlis_algorithm.py" line="99"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
</context>
<context>
    <name>ExportMascaretAlgorithm</name>
    <message>
        <location filename="../processing/export_mascaret_algorithm.py" line="25"/>
        <source>Input</source>
        <translation>Entrée</translation>
    </message>
    <message>
        <location filename="../processing/export_mascaret_algorithm.py" line="34"/>
        <source>Reach name (default to input layer name)</source>
        <translation>Nom du bief (nom de la couche d&apos;entrée par défaut)</translation>
    </message>
    <message>
        <location filename="../processing/export_mascaret_algorithm.py" line="42"/>
        <source>Output file</source>
        <translation>Ficher de sortie</translation>
    </message>
    <message>
        <location filename="../processing/export_mascaret_algorithm.py" line="59"/>
        <source>Reach name cannot contain spaces</source>
        <translation>Le nom du bief ne peut pas contenir d&apos;espaces</translation>
    </message>
    <message>
        <location filename="../processing/export_mascaret_algorithm.py" line="84"/>
        <source>Export Mascaret</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/export_mascaret_algorithm.py" line="87"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
</context>
<context>
    <name>ExportMobiliAlgorithm</name>
    <message>
        <location filename="../processing/export_mobili_algorithm.py" line="65"/>
        <source>Reach name cannot contain spaces</source>
        <translation>Le nom du bief ne peut pas contenir d&apos;espaces</translation>
    </message>
    <message>
        <location filename="../processing/export_mobili_algorithm.py" line="135"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <location filename="../processing/export_mobili_algorithm.py" line="132"/>
        <source>Export Mobili</source>
        <translation>Export Mobili</translation>
    </message>
    <message>
        <location filename="../processing/export_mobili_algorithm.py" line="47"/>
        <source>Output folder</source>
        <translation>Dossier de sortie</translation>
    </message>
    <message>
        <location filename="../processing/export_mobili_algorithm.py" line="30"/>
        <source>Profiles layer</source>
        <translation>Couche profils</translation>
    </message>
    <message>
        <location filename="../processing/export_mobili_algorithm.py" line="103"/>
        <source>You&apos;re not supposed to have more than 3 z interfaces!</source>
        <translation>Vous n&apos;êtes pas censés avoir plus de 3 interfaces !</translation>
    </message>
    <message>
        <location filename="../processing/export_mobili_algorithm.py" line="70"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../processing/export_mobili_algorithm.py" line="70"/>
        <source>Export Mobili: You&apos;re not supposed to have more than 3 z interfaces!</source>
        <translation>Export Mobili: Vous n&apos;êtes pas censés avoir plus de 3 interfaces en Z !</translation>
    </message>
    <message>
        <location filename="../processing/export_mobili_algorithm.py" line="39"/>
        <source>Reach name (default to Profiles layer name)</source>
        <translation>Nom du bief (par défaut nom de la couche profils)</translation>
    </message>
</context>
<context>
    <name>ExportVisuProfilAlgorithm</name>
    <message>
        <location filename="../processing/export_visu_profil_algorithm.py" line="45"/>
        <source>Input profiles layer</source>
        <translation>Couche profils en entrée</translation>
    </message>
    <message>
        <location filename="../processing/export_visu_profil_algorithm.py" line="54"/>
        <source>Output excel file</source>
        <translation>Fichier Excel en sortie</translation>
    </message>
    <message>
        <location filename="../processing/export_visu_profil_algorithm.py" line="219"/>
        <source>Export VisuProfil</source>
        <translation>Export VisuProfil</translation>
    </message>
    <message>
        <location filename="../processing/export_visu_profil_algorithm.py" line="222"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <location filename="../processing/export_visu_profil_algorithm.py" line="80"/>
        <source>Load precourlis file from input profiles layer</source>
        <translation>Charger le fichier precourlis à partir de la couche de profils d&apos;entrée</translation>
    </message>
    <message>
        <location filename="../processing/export_visu_profil_algorithm.py" line="88"/>
        <source>Load excel workbook</source>
        <translation>Charger un classeur Excel</translation>
    </message>
    <message>
        <location filename="../processing/export_visu_profil_algorithm.py" line="104"/>
        <source>Write name of each profiles / Z interfaces</source>
        <translation>Écrire le nom de chaque profil / interface Z</translation>
    </message>
    <message>
        <location filename="../processing/export_visu_profil_algorithm.py" line="137"/>
        <source>Write longitudinal abscissa, section name an profil name in column</source>
        <translation>Écrire en abscisse longitudinale, le nom de la section et le nom du profil en colonne</translation>
    </message>
    <message>
        <location filename="../processing/export_visu_profil_algorithm.py" line="170"/>
        <source>Clear rows</source>
        <translation>Effacer les lignes</translation>
    </message>
    <message>
        <location filename="../processing/export_visu_profil_algorithm.py" line="207"/>
        <source>Save excel file</source>
        <translation>Enregistrer le fichier Excel</translation>
    </message>
    <message>
        <location filename="../processing/export_visu_profil_algorithm.py" line="94"/>
        <source>Load worksheet named &apos;Affichage&apos;</source>
        <translation>Charger la feuille de travail nommée &apos;Affichage&apos;</translation>
    </message>
    <message>
        <location filename="../processing/export_visu_profil_algorithm.py" line="160"/>
        <source>Load worksheet named &apos;Données&apos;</source>
        <translation>Charger la feuille de travail nommée &apos;Données&apos;</translation>
    </message>
    <message>
        <location filename="../processing/export_visu_profil_algorithm.py" line="182"/>
        <source>Write in &apos;Données&apos; worksheet</source>
        <translation>Ecrire dans la feuille de travail &apos;Données&apos;</translation>
    </message>
</context>
<context>
    <name>GeoreferencingAlgorithm</name>
    <message>
        <location filename="../processing/georeferencing_algorithm.py" line="25"/>
        <source>Input .geo file</source>
        <translation>Fichier .geo en entrée</translation>
    </message>
    <message>
        <location filename="../processing/georeferencing_algorithm.py" line="47"/>
        <source>Output .georef file</source>
        <translation>Fichier de sortie .georef</translation>
    </message>
    <message>
        <location filename="../processing/georeferencing_algorithm.py" line="55"/>
        <source>Output layer</source>
        <translation>Couche de sortie</translation>
    </message>
    <message>
        <location filename="../processing/georeferencing_algorithm.py" line="195"/>
        <source>georeferencing</source>
        <translation>Géoréférencement</translation>
    </message>
    <message>
        <location filename="../processing/georeferencing_algorithm.py" line="29"/>
        <source>Tracks</source>
        <translation>Traces</translation>
    </message>
    <message>
        <location filename="../processing/georeferencing_algorithm.py" line="33"/>
        <source>Alignment</source>
        <translation>Alignement</translation>
    </message>
    <message>
        <location filename="../processing/georeferencing_algorithm.py" line="33"/>
        <source>left</source>
        <translation>gauche</translation>
    </message>
    <message>
        <location filename="../processing/georeferencing_algorithm.py" line="33"/>
        <source>axis</source>
        <translation>axe</translation>
    </message>
</context>
<context>
    <name>ImportGeorefAlgorithm</name>
    <message>
        <location filename="../processing/import_georef_algorithm.py" line="28"/>
        <source>Input file</source>
        <translation>Fichier d&apos;entrée</translation>
    </message>
    <message>
        <location filename="../processing/import_georef_algorithm.py" line="35"/>
        <source>GeorefMascaret</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/import_georef_algorithm.py" line="85"/>
        <source>Import Georef</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/import_georef_algorithm.py" line="29"/>
        <source>Coordinates projection system</source>
        <translation>Système de projection</translation>
    </message>
</context>
<context>
    <name>ImportLayerFromDemAlgorithm</name>
    <message>
        <location filename="../processing/import_layer_from_dem_algorithm.py" line="39"/>
        <source>Layer name</source>
        <translation>Nom de la couche</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_dem_algorithm.py" line="44"/>
        <source>Digital Elevation Model</source>
        <translation>Modèle numérique de terrain</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_dem_algorithm.py" line="76"/>
        <source>Band number</source>
        <translation>Numéro de la bande</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_dem_algorithm.py" line="194"/>
        <source>Import layer from DEM</source>
        <translation>Importer une couche depuis un MNT</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_dem_algorithm.py" line="204"/>
        <source>Import</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_dem_algorithm.py" line="31"/>
        <source>Input</source>
        <translation>Entrée</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_dem_algorithm.py" line="66"/>
        <source>Default elevation (the default value when there is no Z value to extract)</source>
        <translation>Altitude par défaut (valeur par défaut quand il n&apos;y a aucune valeur à extraire pour le Z)</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_dem_algorithm.py" line="49"/>
        <source>Mask</source>
        <translation>Masque</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_dem_algorithm.py" line="57"/>
        <source>Points filter (formatted as a json array or arrays : [ [sec_id, p_id], ... ])</source>
        <translation>Filtre sur les points (formaté sous forme de tableaux json ou de tableux: [ [sec_id, p_id], ... ])</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_dem_algorithm.py" line="197"/>
        <source>This algorithm import/extract the elevation value from a Digital Elevation Model (DEM).

We can specify the area of interest with a list of points and/or a mask.</source>
        <translation>Cet algorithme importe/extrait la valeur d&apos;élévation d&apos;un modèle numérique d&apos;élévation (DEM).

Nous pouvons préciser la zone d&apos;intérêt avec une liste de points et/ou un masque.</translation>
    </message>
</context>
<context>
    <name>ImportLayerFromPointsAlgorithm</name>
    <message>
        <location filename="../processing/import_layer_from_points_algorithm.py" line="30"/>
        <source>Target profiles</source>
        <translation>Profils cibles</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_points_algorithm.py" line="38"/>
        <source>Target layer to fill</source>
        <translation>Couche cible à remplir</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_points_algorithm.py" line="46"/>
        <source>Source points</source>
        <translation>Points sources</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_points_algorithm.py" line="54"/>
        <source>Source section name field</source>
        <translation>Champ du nom de la section source</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_points_algorithm.py" line="62"/>
        <source>Source Z field</source>
        <translation>Champ source Z</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_points_algorithm.py" line="70"/>
        <source>Mask</source>
        <translation>Masque</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_points_algorithm.py" line="78"/>
        <source>Points filter (formatted as a json array or arrays : [ [sec_id, p_id], ... ])</source>
        <translation>Filtre sur les points (formaté sous forme de tableaux json ou de tableux: [ [sec_id, p_id], ... ])</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_points_algorithm.py" line="87"/>
        <source>Default elevation (the default value when there is no Z value to extract)</source>
        <translation>Altitude par défaut (valeur par défaut quand il n&apos;y a aucune valeur à extraire pour le Z)</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_points_algorithm.py" line="264"/>
        <source>Import layer from points</source>
        <translation>Importer une couche à partir de points</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_points_algorithm.py" line="267"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_points_algorithm.py" line="276"/>
        <source>This algorithm fill or create a new sedimental layer interface in target profiles from source points with elevation and already having some field making the relation with tracks.

Note that it is possible to create the relation between points and tracks using function &apos;Join points to profiles&apos;.</source>
        <translation>Cet algorithme remplit ou crée une nouvelle interface de couche sédimentaire dans des profils cibles à partir de points sources avec élévation et disposant déjà d&apos;un champ faisant la relation avec les traces.

A noter qu&apos;il est possible de créer la relation entre points et traces à l&apos;aide de la fonction &apos;Joindre les points aux profils&apos;.</translation>
    </message>
</context>
<context>
    <name>ImportLayerFromProfilesAlgorithm</name>
    <message>
        <location filename="../processing/import_layer_from_profiles_algorithm.py" line="41"/>
        <source>Target profiles</source>
        <translation>Profils cibles</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_profiles_algorithm.py" line="49"/>
        <source>Target layer to fill</source>
        <translation>Couche cible à remplir</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_profiles_algorithm.py" line="57"/>
        <source>Source profiles</source>
        <translation>Profils cibles</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_profiles_algorithm.py" line="62"/>
        <source>Source layer field</source>
        <translation>Champ couche source</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_profiles_algorithm.py" line="70"/>
        <source>Import source points</source>
        <translation>Importer des points sources</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_profiles_algorithm.py" line="77"/>
        <source>Mask</source>
        <translation>Masque</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_profiles_algorithm.py" line="85"/>
        <source>Points filter (formatted as a json array or arrays : [ [sec_id, p_id], ... ])</source>
        <translation>Filtre sur les points (formaté sous forme de tableaux json ou de tableux: [ [sec_id, p_id], ... ])</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_profiles_algorithm.py" line="94"/>
        <source>Default elevation (the default value when there is no Z value to extract)</source>
        <translation>Altitude par défaut (valeur par défaut quand il n&apos;y a aucune valeur à extraire pour le Z)</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_profiles_algorithm.py" line="464"/>
        <source>Import layer from profiles</source>
        <translation>Importer une couche à partir de profils</translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_profiles_algorithm.py" line="467"/>
        <source>Import</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/import_layer_from_profiles_algorithm.py" line="476"/>
        <source>This algorithm fill or create a new sedimental layer interface in target profiles from another profiles layer.</source>
        <translation>Cet algorithme remplit ou crée une nouvelle interface de couche sédimentaire dans des profils cibles à partir d&apos;une autre couche de profils.</translation>
    </message>
</context>
<context>
    <name>ImportPointsAlgorithm</name>
    <message>
        <location filename="../processing/import_points_algorithm.py" line="24"/>
        <source>Tracks</source>
        <translation>Traces</translation>
    </message>
    <message>
        <location filename="../processing/import_points_algorithm.py" line="40"/>
        <source>Axis</source>
        <translation>Axe</translation>
    </message>
    <message>
        <location filename="../processing/import_points_algorithm.py" line="48"/>
        <source>Source points</source>
        <translation>Points sources</translation>
    </message>
    <message>
        <location filename="../processing/import_points_algorithm.py" line="56"/>
        <source>Source Z field</source>
        <translation>Champ source Z</translation>
    </message>
    <message>
        <location filename="../processing/import_points_algorithm.py" line="64"/>
        <source>Buffer distance</source>
        <translation>Distance tampon</translation>
    </message>
    <message>
        <location filename="../processing/import_points_algorithm.py" line="74"/>
        <source>Sections</source>
        <translation>Sections</translation>
    </message>
    <message>
        <location filename="../processing/import_points_algorithm.py" line="134"/>
        <source>Import points by projection on tracks</source>
        <translation>Importer des points par projection des traces</translation>
    </message>
    <message>
        <location filename="../processing/import_points_algorithm.py" line="137"/>
        <source>Import</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/import_points_algorithm.py" line="146"/>
        <source>This algorithm create a new profiles layer from:
- Tracks
- Source points with elevation

using a buffer to join points to tracks and then projecting points on tracks.</source>
        <translation>Cet algorithme crée une nouvelle couche de profils à partir&#xa0;:
- Des traces
- Des points sources avec une élévation

Utiliser un tampon pour joindre des points aux traces, puis projeter des points sur les traces.</translation>
    </message>
    <message>
        <location filename="../processing/import_points_algorithm.py" line="32"/>
        <source>Tracks unique identifier field</source>
        <translation>Champ identifiant unique de la couche traces</translation>
    </message>
</context>
<context>
    <name>ImportPreparedPointsAlgorithm</name>
    <message>
        <location filename="../processing/import_prepared_points_algorithm.py" line="31"/>
        <source>Tracks</source>
        <translation>Traces</translation>
    </message>
    <message>
        <location filename="../processing/import_prepared_points_algorithm.py" line="47"/>
        <source>Axis</source>
        <translation>Axe</translation>
    </message>
    <message>
        <location filename="../processing/import_prepared_points_algorithm.py" line="55"/>
        <source>Source points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/import_prepared_points_algorithm.py" line="71"/>
        <source>Source Z field</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/import_prepared_points_algorithm.py" line="79"/>
        <source>Sections</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/import_prepared_points_algorithm.py" line="227"/>
        <source>Import prepared points by projection on tracks</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/import_prepared_points_algorithm.py" line="230"/>
        <source>Import</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/import_prepared_points_algorithm.py" line="239"/>
        <source>This algorithm create a new profiles layer by projecting points on tracks using:
- Tracks
- Source points with elevation already having some field making the relation with tracks

Note that it is possible to create the relation between points and tracks using function &apos;Join points to profiles&apos;.</source>
        <translation>Cet algorithme crée une nouvelle couche de profils en projetant des points sur les traces en utilisant&#xa0;:
- Des traces
- Des points sources avec une élévation ayant déjà un champ faisant la relation avec les traces

A noter qu&apos;il est possible de créer la relation entre points et traces à l&apos;aide de la fonction &apos;Joindre les points aux profils&apos;.</translation>
    </message>
    <message>
        <location filename="../processing/import_prepared_points_algorithm.py" line="39"/>
        <source>Tracks identifier field</source>
        <translation>Champ identifiant de la couche traces</translation>
    </message>
    <message>
        <location filename="../processing/import_prepared_points_algorithm.py" line="63"/>
        <source>Source tracks identifier field</source>
        <translation>Champ identifiant des traces dans la couche source</translation>
    </message>
</context>
<context>
    <name>ImportTracksAlgorithm</name>
    <message>
        <location filename="../processing/import_tracks_algorithm.py" line="32"/>
        <source>Tracks</source>
        <translation>Traces</translation>
    </message>
    <message>
        <location filename="../processing/import_tracks_algorithm.py" line="40"/>
        <source>Axis</source>
        <translation>Axe</translation>
    </message>
    <message>
        <location filename="../processing/import_tracks_algorithm.py" line="48"/>
        <source>Abscissa of first section</source>
        <translation>Abscisse de la première section</translation>
    </message>
    <message>
        <location filename="../processing/import_tracks_algorithm.py" line="57"/>
        <source>Abscissa of axis first point (take precedence over abscissa of first section when set)</source>
        <translation>Abscisse du premier point de l&apos;axe (prend le pas sur l&apos;abscisse de la première section si définit)</translation>
    </message>
    <message>
        <location filename="../processing/import_tracks_algorithm.py" line="68"/>
        <source>Name field</source>
        <translation>Champ nom</translation>
    </message>
    <message>
        <location filename="../processing/import_tracks_algorithm.py" line="86"/>
        <source>Maximum distance between two points</source>
        <translation>Distance maximale entre deux points</translation>
    </message>
    <message>
        <location filename="../processing/import_tracks_algorithm.py" line="94"/>
        <source>Apply strict distance (do not keep initial points)</source>
        <translation>Appliquer une distance stricte (ne garde pas les points initiaux)</translation>
    </message>
    <message>
        <location filename="../processing/import_tracks_algorithm.py" line="101"/>
        <source>Digital Elevation Model</source>
        <translation>Modèle numérique de terrain</translation>
    </message>
    <message>
        <location filename="../processing/import_tracks_algorithm.py" line="339"/>
        <source>Sections</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/import_tracks_algorithm.py" line="366"/>
        <source>Import tracks</source>
        <translation>Import de traces</translation>
    </message>
    <message>
        <location filename="../processing/import_tracks_algorithm.py" line="369"/>
        <source>Import</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/import_tracks_algorithm.py" line="109"/>
        <source>Default elevation (the default value when there is no Z value to extract)</source>
        <translation>Altitude par défaut (valeur par défaut quand il n&apos;y a aucune valeur à extraire pour le Z)</translation>
    </message>
    <message>
        <location filename="../processing/import_tracks_algorithm.py" line="79"/>
        <source>Lateral interpolation</source>
        <translation>Interpolation latérale</translation>
    </message>
</context>
<context>
    <name>InterpolateLinesAlgorithm</name>
    <message>
        <location filename="../processing/interpolate_lines.py" line="32"/>
        <source>Sections</source>
        <translation>Sections</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_lines.py" line="40"/>
        <source>Axis</source>
        <translation>Axe</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_lines.py" line="48"/>
        <source>Contraint lines</source>
        <translation>Lignes de contraintes</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_lines.py" line="68"/>
        <source>Longitudinal space step (in m)</source>
        <translation>Pas longitudinal (en mètres)</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_lines.py" line="76"/>
        <source>Lateral space step (in m)</source>
        <translation>Pas latéral (en mètres)</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_lines.py" line="84"/>
        <source>Attribute to identify cross-sections</source>
        <translation>Attribut identifiant des sections</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_lines.py" line="187"/>
        <source>Interpolated</source>
        <translation>Profils interpolés</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_lines.py" line="214"/>
        <source>Interpolate lines</source>
        <translation>Interpoler des lignes</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_lines.py" line="217"/>
        <source>Interpolate</source>
        <translation>Interpolation</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_lines.py" line="92"/>
        <source>Interpolated profiles</source>
        <translation>Profils interpolés</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_lines.py" line="57"/>
        <source>Create constraint lines at profiles ends (avoid errors when some constraint lines doesn&apos;t cross all profiles)</source>
        <translation>Créer des lignes de contraintes aux extrémites des profils (évite des erreurs lorsque certaines lignes de contraintes ne croisent pas tous les profils)</translation>
    </message>
</context>
<context>
    <name>InterpolatePointsAlgorithm</name>
    <message>
        <location filename="../processing/interpolate_points.py" line="73"/>
        <source>Sections</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/interpolate_points.py" line="81"/>
        <source>Axis</source>
        <translation>Axe</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_points.py" line="89"/>
        <source>Contraint lines</source>
        <translation>Lignes de contraintes</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_points.py" line="109"/>
        <source>Longitudinal space step (in m)</source>
        <translation>Pas longitudinal (en mètres)</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_points.py" line="116"/>
        <source>Lateral space step (in m)</source>
        <translation>Pas latéral (en mètres)</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_points.py" line="124"/>
        <source>Attribute to identify cross-sections</source>
        <translation>Attribut identifiant des sections</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_points.py" line="132"/>
        <source>Interpolated</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/interpolate_points.py" line="326"/>
        <source>Interpolate points</source>
        <translation>Interpoler des points</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_points.py" line="329"/>
        <source>Interpolate</source>
        <translation>Interpolation</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_points.py" line="98"/>
        <source>Create constraint lines at profiles ends (avoid errors when some constraint lines doesn&apos;t cross all profiles)</source>
        <translation>Créer des lignes de contraintes aux extrémites des profils (évite des erreurs lorsque certaines lignes de contraintes ne croisent pas tous les profils)</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_points.py" line="310"/>
        <source>Failed to execute command {}</source>
        <translation>Échec de l&apos;exécution de la commande {}</translation>
    </message>
</context>
<context>
    <name>InterpolateProfilesPerAreasAlgorithm</name>
    <message>
        <location filename="../processing/interpolate_profiles_per_areas_algorithm.py" line="104"/>
        <source>Lateral space step (in m)</source>
        <translation>Pas latéral (en mètres)</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_per_areas_algorithm.py" line="120"/>
        <source>Interpolated profiles</source>
        <translation>Profils interpolés</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_per_areas_algorithm.py" line="284"/>
        <source>Interpolate profiles per areas</source>
        <translation>Interpoler les profils par parties</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_per_areas_algorithm.py" line="287"/>
        <source>Interpolate</source>
        <translation>Interpolation</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_per_areas_algorithm.py" line="69"/>
        <source>Axis</source>
        <translation>Axe</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_per_areas_algorithm.py" line="77"/>
        <source>Contraint lines</source>
        <translation>Lignes de contraintes</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_per_areas_algorithm.py" line="86"/>
        <source>Create constraint lines at profiles ends (avoid errors when some constraint lines doesn&apos;t cross all profiles)</source>
        <translation>Créer des lignes de contraintes aux extrémites des profils (évite des erreurs lorsque certaines lignes de contraintes ne croisent pas tous les profils)</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_per_areas_algorithm.py" line="112"/>
        <source>Attribute to identify cross-sections</source>
        <translation>Attribut identifiant des sections</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_per_areas_algorithm.py" line="61"/>
        <source>Sections</source>
        <translation>Sections</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_per_areas_algorithm.py" line="132"/>
        <source>Sections parameter is not set or invalid</source>
        <translation>Le paramètre sections est indéfini ou invalide</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_per_areas_algorithm.py" line="144"/>
        <source>Axis parameter is not set or invalid</source>
        <translation>Le paramètre axe est indéfini ou invalide</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_per_areas_algorithm.py" line="97"/>
        <source>Areas</source>
        <translation>Parties</translation>
    </message>
</context>
<context>
    <name>InterpolateProfilesWithoutLateralResamplingAlgorithm</name>
    <message>
        <location filename="../processing/interpolate_profiles_without_lateral_resampling_algorithm.py" line="34"/>
        <source>Sections</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_without_lateral_resampling_algorithm.py" line="42"/>
        <source>Axis</source>
        <translation>Axe</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_without_lateral_resampling_algorithm.py" line="50"/>
        <source>Longitudinal space step (in m)</source>
        <translation>Pas longitudinal (en mètres)</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_without_lateral_resampling_algorithm.py" line="58"/>
        <source>Attribute to identify cross-sections</source>
        <translation>Attribut identifiant des sections</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_without_lateral_resampling_algorithm.py" line="66"/>
        <source>Interpolated profiles</source>
        <translation>Profils interpolés</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_without_lateral_resampling_algorithm.py" line="137"/>
        <source>Profiles {first_section[&apos;sec_name&apos;]} and {last_section[&apos;sec_name&apos;]} do not have the same number of points.</source>
        <translation>Les profils {first_section[&apos;sec_name&apos;]} et {last_section[&apos;sec_name&apos;]} n&apos;ont pas le même nombre de points.</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_without_lateral_resampling_algorithm.py" line="303"/>
        <source>Interpolate profiles without lateral resampling</source>
        <translation>Interpoler des profils sans rééchantillonnage latéral</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_without_lateral_resampling_algorithm.py" line="306"/>
        <source>Interpolate</source>
        <translation>Interpolation</translation>
    </message>
    <message>
        <location filename="../processing/interpolate_profiles_without_lateral_resampling_algorithm.py" line="315"/>
        <source>This algorithm interpolate a profiles layer by creating regular and perpendicular profiles along axis with fixed number of points per profile.

Note that the source profiles must have the same number of points and be perpendical to the axis.</source>
        <translation>Cet algorithme interpole une couche de profils en créant des profils réguliers et perpendiculaires le long d&apos;un axe avec un nombre fixe de points par profil.

Notez que les profils sources doivent avoir le même nombre de points et être perpendiculaires à l&apos;axe.</translation>
    </message>
</context>
<context>
    <name>InterpolationAreasConfigDialog</name>
    <message>
        <location filename="../widgets/interpolation_areas_config_dialog.py" line="156"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../ui/interpolation_areas_config_dialog.ui" line="20"/>
        <source>Inperpolation areas configuration</source>
        <translation>Configuration de l&apos;interpolation par parties</translation>
    </message>
    <message>
        <location filename="../ui/interpolation_areas_config_dialog.ui" line="38"/>
        <source>Profiles</source>
        <translation>Profils</translation>
    </message>
    <message>
        <location filename="../ui/interpolation_areas_config_dialog.ui" line="65"/>
        <source>Add row</source>
        <translation>Ajouter une ligne</translation>
    </message>
    <message>
        <location filename="../ui/interpolation_areas_config_dialog.ui" line="75"/>
        <source>Remove row(s)</source>
        <translation>Retirer des lignes</translation>
    </message>
</context>
<context>
    <name>InterpolationAreasTableModel</name>
    <message>
        <location filename="../widgets/interpolation_areas_table_model.py" line="115"/>
        <source>from_profil_id</source>
        <translation>Profil de début</translation>
    </message>
    <message>
        <location filename="../widgets/interpolation_areas_table_model.py" line="117"/>
        <source>to_profil_id</source>
        <translation>Profil de fin</translation>
    </message>
    <message>
        <location filename="../widgets/interpolation_areas_table_model.py" line="119"/>
        <source>longitudinal_step</source>
        <translation>Pas longitudinal</translation>
    </message>
</context>
<context>
    <name>InterpolationAreasWidgetWrapper</name>
    <message>
        <location filename="../processing/ui/interpolation_areas_widget_wrapper.py" line="27"/>
        <source>Configure interpolation areas</source>
        <translation>Configuration de l&apos;interpolation par parties</translation>
    </message>
</context>
<context>
    <name>JoinPointsToProfilesAlgorithm</name>
    <message>
        <location filename="../processing/join_points_to_profiles_algorithm.py" line="26"/>
        <source>Source points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/join_points_to_profiles_algorithm.py" line="34"/>
        <source>Target profiles</source>
        <translation>Profils cibles</translation>
    </message>
    <message>
        <location filename="../processing/join_points_to_profiles_algorithm.py" line="51"/>
        <source>Buffer distance</source>
        <translation>Distance tampon</translation>
    </message>
    <message>
        <location filename="../processing/join_points_to_profiles_algorithm.py" line="61"/>
        <source>Points prepared</source>
        <translation>Points préparés</translation>
    </message>
    <message>
        <location filename="../processing/join_points_to_profiles_algorithm.py" line="157"/>
        <source>Join points to profiles</source>
        <translation>Joindre les points aux profils</translation>
    </message>
    <message>
        <location filename="../processing/join_points_to_profiles_algorithm.py" line="160"/>
        <source>Import</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/join_points_to_profiles_algorithm.py" line="169"/>
        <source>This algorithm create a relation between points and profiles using a buffer around profiles.</source>
        <translation>Cet algorithme crée une relation entre les points et les profils en utilisant un tampon autour des profils.</translation>
    </message>
    <message>
        <location filename="../processing/join_points_to_profiles_algorithm.py" line="42"/>
        <source>Target profiles unique identifier field</source>
        <translation>Champs identifiant unique des profils cibles</translation>
    </message>
</context>
<context>
    <name>LinesToPointsAlgorithm</name>
    <message>
        <location filename="../processing/lines_to_points_algorithm.py" line="24"/>
        <source>Input</source>
        <translation>Entrée</translation>
    </message>
    <message>
        <location filename="../processing/lines_to_points_algorithm.py" line="31"/>
        <source>Points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/lines_to_points_algorithm.py" line="111"/>
        <source>Lines to points</source>
        <translation>Lignes vers points</translation>
    </message>
    <message>
        <location filename="../processing/lines_to_points_algorithm.py" line="114"/>
        <source>Convert</source>
        <translation>Conversion</translation>
    </message>
</context>
<context>
    <name>NewPointDialog</name>
    <message>
        <location filename="../widgets/new_point_dialog.py" line="331"/>
        <source>Field topo_bat is required</source>
        <translation>Le champ topo_bat est obligatoire</translation>
    </message>
    <message>
        <location filename="../widgets/new_point_dialog.py" line="337"/>
        <source>Validation error</source>
        <translation>Erreur de validation</translation>
    </message>
</context>
<context>
    <name>PointsAlongLinesAlgorithm</name>
    <message>
        <location filename="../processing/points_along_lines_algorithm.py" line="24"/>
        <source>Distance</source>
        <translation>Distance</translation>
    </message>
    <message>
        <location filename="../processing/points_along_lines_algorithm.py" line="36"/>
        <source>Interpolated points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/points_along_lines_algorithm.py" line="96"/>
        <source>Points along lines</source>
        <translation>Points le long d&apos;une ligne</translation>
    </message>
    <message>
        <location filename="../processing/points_along_lines_algorithm.py" line="99"/>
        <source>Interpolate</source>
        <translation>Interpolation</translation>
    </message>
</context>
<context>
    <name>PointsToLinesAlgorithm</name>
    <message>
        <location filename="../processing/points_to_lines_algorithm.py" line="32"/>
        <source>Input</source>
        <translation>Entrée</translation>
    </message>
    <message>
        <location filename="../processing/points_to_lines_algorithm.py" line="39"/>
        <source>Axis (needed to calculate axis_x and axis_y if not already set in source)</source>
        <translation>Axe</translation>
    </message>
    <message>
        <location filename="../processing/points_to_lines_algorithm.py" line="50"/>
        <source>Abscissa of first section</source>
        <translation>Abscisse de la première section</translation>
    </message>
    <message>
        <location filename="../processing/points_to_lines_algorithm.py" line="59"/>
        <source>Abscissa of axis first point (take precedence over abscissa of first section when set)</source>
        <translation>Abscisse du premier point de l&apos;axe (prend le pas sur l&apos;abscisse de la première section si définit)</translation>
    </message>
    <message>
        <location filename="../processing/points_to_lines_algorithm.py" line="70"/>
        <source>Group by field</source>
        <translation>Champ de regroupement</translation>
    </message>
    <message>
        <location filename="../processing/points_to_lines_algorithm.py" line="80"/>
        <source>Order by field</source>
        <translation>Champ de tri</translation>
    </message>
    <message>
        <location filename="../processing/points_to_lines_algorithm.py" line="90"/>
        <source>Lines</source>
        <translation>Lignes</translation>
    </message>
    <message>
        <location filename="../processing/points_to_lines_algorithm.py" line="257"/>
        <source>Points to lines</source>
        <translation>Points vers lignes</translation>
    </message>
    <message>
        <location filename="../processing/points_to_lines_algorithm.py" line="260"/>
        <source>Convert</source>
        <translation>Conversion</translation>
    </message>
</context>
<context>
    <name>PreCourlisPlugin</name>
    <message>
        <location filename="../PreCourlis.py" line="171"/>
        <source>Georeference and import a .geo file</source>
        <translation>Géoréférencer et importer un fichier .geo</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="176"/>
        <source>Import a .georef / .georefC file</source>
        <translation>Importer un fichier .georef / .georefC</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="181"/>
        <source>Reverse the direction of the hydraulic axis</source>
        <translation>Inverser le sens de l&apos;axe hydraulique</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="186"/>
        <source>Convert tracks to profiles</source>
        <translation>Convertir les traces en profils</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="204"/>
        <source>View profiles</source>
        <translation>Visualiser les profils</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="251"/>
        <source>Interpolate profiles</source>
        <translation>Interpoler des profils</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="256"/>
        <source>Configure interpolation areas</source>
        <translation>Configuration de l&apos;interpolation par parties</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="261"/>
        <source>Interpolate profiles with areas</source>
        <translation>Interpoler les profils par parties</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="275"/>
        <source>Export a Courlis geometry file</source>
        <translation>Exporter un fichier de géométrie Courlis</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="280"/>
        <source>Export a Mascaret geometry file</source>
        <translation>Exporter un fichier de géométrie Mascaret</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="285"/>
        <source>Export a Mobili geometry file</source>
        <translation>Exporter un fichier de géométrie Mobili</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="298"/>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="299"/>
        <source>About</source>
        <translation>A propos</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="290"/>
        <source>Export a VisuProfil file</source>
        <translation>Exporter un fichier VisuProfil</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="191"/>
        <source>Convert points to profiles</source>
        <translation>Convertir les points en profils</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="213"/>
        <source>Add intermediate profiles</source>
        <translation>Ajouter les profils intermédiaires</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="221"/>
        <source>Join points to profiles</source>
        <translation>Joindre les points aux profils</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="221"/>
        <source>This algorithm fill raw points with corresponding profile identifier using a buffer around profiles.</source>
        <translation>Cet algorithme remplit les points bruts avec l&apos;identifiant de profil correspondant en utilisant un tampon autour des profils.</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="231"/>
        <source>Import sedimental interface from points</source>
        <translation>Importer une interface sédimentaire à partir de points</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="231"/>
        <source>This algorithm fill a sedimental interface altitude from points.</source>
        <translation>Cet algorithme remplit une altitude d&apos;interface sédimentaire à partir de points.</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="240"/>
        <source>Define topography / bathymetry from a polygon</source>
        <translation>Définir la topographie/bathymétrie à partir d&apos;un polygone</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="240"/>
        <source>This algorithm define topography / bathymetry from a polygon.</source>
        <translation>Cet algorithme définit la topographie/bathymétrie à partir d&apos;un polygone.</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="267"/>
        <source>Interpolate without lateral resampling</source>
        <translation>Interpoler sans rééchantillonnage latéral</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="306"/>
        <source>Start debugpy and wait for client</source>
        <translation>Démarrez le débogage et attendez le client</translation>
    </message>
    <message>
        <location filename="../PreCourlis.py" line="196"/>
        <source>Repair profiles layer</source>
        <translation>Réparer une couche de profils</translation>
    </message>
</context>
<context>
    <name>PreCourlisProvider</name>
    <message>
        <location filename="../processing/precourlis_provider.py" line="153"/>
        <source>PreCourlis</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PrepareTracksAlgorithm</name>
    <message>
        <location filename="../processing/prepare_tracks_algorithm.py" line="38"/>
        <source>Tracks</source>
        <translation>Traces</translation>
    </message>
    <message>
        <location filename="../processing/prepare_tracks_algorithm.py" line="45"/>
        <source>Axis</source>
        <translation>Axe</translation>
    </message>
    <message>
        <location filename="../processing/prepare_tracks_algorithm.py" line="52"/>
        <source>Name field</source>
        <translation>Champ nom</translation>
    </message>
    <message>
        <location filename="../processing/prepare_tracks_algorithm.py" line="61"/>
        <source>Prepared</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/prepare_tracks_algorithm.py" line="154"/>
        <source>Prepare tracks</source>
        <translation>Préparer les traces</translation>
    </message>
    <message>
        <location filename="../processing/prepare_tracks_algorithm.py" line="157"/>
        <source>Import</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ProfileDialog</name>
    <message>
        <location filename="../widgets/profile_dialog.py" line="748"/>
        <source>Save Layer Edits</source>
        <translation>Enregistrer les modifications</translation>
    </message>
    <message>
        <location filename="../widgets/profile_dialog.py" line="748"/>
        <source>Could not commit changes to layer {}

Errors: {}
</source>
        <translation>Impossible d&apos;enregistrer les modifications à la couche {}

Erreurs: {}
</translation>
    </message>
    <message>
        <location filename="../widgets/profile_dialog.py" line="327"/>
        <source>Not set</source>
        <translation>Non définit</translation>
    </message>
    <message>
        <location filename="../widgets/profile_dialog.py" line="312"/>
        <source>DEM</source>
        <translation>DEM</translation>
    </message>
    <message>
        <location filename="../widgets/profile_dialog.py" line="313"/>
        <source>Profiles</source>
        <translation>Profils</translation>
    </message>
    <message>
        <location filename="../widgets/profile_dialog.py" line="316"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../widgets/profile_dialog.py" line="620"/>
        <source>Add new point</source>
        <translation>Ajouter un nouveau point</translation>
    </message>
    <message>
        <location filename="../widgets/profile_dialog.py" line="620"/>
        <source>Please select one and only one point</source>
        <translation>Veuillez sélectionner un et un seul point</translation>
    </message>
    <message>
        <location filename="../widgets/profile_dialog.py" line="635"/>
        <source>Point added to profile</source>
        <translation>Point ajouté au profil</translation>
    </message>
    <message>
        <location filename="../widgets/profile_dialog.py" line="654"/>
        <source>Remove points</source>
        <translation>Supprimer des points</translation>
    </message>
    <message>
        <location filename="../widgets/profile_dialog.py" line="654"/>
        <source>No points selected</source>
        <translation>Aucun point sélectionné</translation>
    </message>
    <message>
        <location filename="../widgets/profile_dialog.py" line="676"/>
        <source>Points removed from profile</source>
        <translation>Points supprimés du profil</translation>
    </message>
    <message>
        <location filename="../widgets/profile_dialog.py" line="680"/>
        <source>Invalid points from section moved up</source>
        <translation>Points invalides de la section déplacée vers le haut</translation>
    </message>
    <message>
        <location filename="../widgets/profile_dialog.py" line="732"/>
        <source>Invalid points from layer moved down</source>
        <translation>Points invalides de la couche déplacés vers le bas</translation>
    </message>
    <message>
        <location filename="../widgets/profile_dialog.py" line="382"/>
        <source>Missing field</source>
        <translation>Champs manquant</translation>
    </message>
    <message>
        <location filename="../widgets/profile_dialog.py" line="382"/>
        <source>The field &apos;topo_bat&apos; is missing in the layer. Please add it with the repair profiles algorithm before using this tool.</source>
        <translation>Le champ &apos;topo_bat&apos; est manquant dans la couche. Veuillez l&apos;ajouter en utilisant l&apos;algorithme Réparer les profils avant d&apos;utiliser cet outil.</translation>
    </message>
    <message>
        <location filename="../widgets/profile_dialog.py" line="684"/>
        <source>Invalid points from section moved down</source>
        <translation>Les points invalides de la section ont été déplacés vers le bas</translation>
    </message>
    <message>
        <location filename="../widgets/profile_dialog.py" line="706"/>
        <source>Invalid points from layer moved up</source>
        <translation>Les points invalides de la couche ont été déplacés vers le haut</translation>
    </message>
</context>
<context>
    <name>ProfileDialogBase</name>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="14"/>
        <source>Profiles editor</source>
        <translation>Editeur de profils</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="44"/>
        <source>&lt;&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="67"/>
        <source>&gt;&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="376"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="348"/>
        <source>Sedimental layers</source>
        <translation>Couches sédimentaires</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="427"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="608"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="447"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="544"/>
        <source>Interpolation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="552"/>
        <source>Left</source>
        <translation>Gauche</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="575"/>
        <source>Right</source>
        <translation>Droite</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="618"/>
        <source>Clear</source>
        <translation>RAZ</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="84"/>
        <source>Undo</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="94"/>
        <source>Redo</source>
        <translation>Refaire</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="31"/>
        <source>Select main layer</source>
        <translation>Sélectionner la couche principale</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="41"/>
        <source>Previous profile</source>
        <translation>Profil précédent</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="64"/>
        <source>Next profile</source>
        <translation>Profil suivant</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="362"/>
        <source>Select sedimental layer</source>
        <translation>Sélectionner la couche sédimentaire</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="397"/>
        <source>Sedimental layer color</source>
        <translation>Couleur de la couche sédimentaire</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="605"/>
        <source>Apply interpolation</source>
        <translation>Appliquer l&apos;interpolation</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="615"/>
        <source>Clear values</source>
        <translation>Remettre les valeurs à zéro</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="104"/>
        <source>Save modifications</source>
        <translation>Enregistrer les modifications</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="424"/>
        <source>Add new sedimental layer</source>
        <translation>Ajouter une nouvelle couche sédimentaire</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="434"/>
        <source>Apply settings to current sedimental layer</source>
        <translation>Appliquer les réglages à la couche sédimentaire courante</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="444"/>
        <source>Delete current sedimental layer</source>
        <translation>Supprimer la couche sédimentaire courante</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="387"/>
        <source>Sedimental layer name (limited to 7 characters)</source>
        <translation>Nom de la couche sédimentaire (limité à 7 caractères)</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="531"/>
        <source>Extract layer values from DEM</source>
        <translation>Extraire les valeurs de la couche depuis un MNT</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="534"/>
        <source>Extract Z</source>
        <translation>Extraire le Z</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="518"/>
        <source>Default elevation</source>
        <translation>Altitude par défaut</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="74"/>
        <source>Delete current profile</source>
        <translation>Supprimer le profil actuel</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="147"/>
        <source>Select secondary layer</source>
        <translation>Sélectionnez le calque secondaire</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="157"/>
        <source>Buffer size to catch secondary profiles</source>
        <translation>Taille du tampon pour capturer les profils secondaires</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="164"/>
        <source>hide/show previous and next sections</source>
        <translation>masquer/afficher les sections précédentes et suivantes</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="186"/>
        <source>Add new point after selected point</source>
        <translation>Ajouter un nouveau point après le point sélectionné</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="196"/>
        <source>Remove selected points</source>
        <translation>Supprimer les points sélectionnés</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="459"/>
        <source>Import Z</source>
        <translation>Importer Z</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="467"/>
        <source>From</source>
        <translation>Depuis</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="484"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../ui/profile_dialog_base.ui" line="501"/>
        <source>Mask</source>
        <translation>Masque</translation>
    </message>
</context>
<context>
    <name>RepairProfilesAlgorithm</name>
    <message>
        <location filename="../processing/repair_profiles_algorithm.py" line="17"/>
        <source>Input</source>
        <translation>Entrée</translation>
    </message>
    <message>
        <location filename="../processing/repair_profiles_algorithm.py" line="26"/>
        <source>Output</source>
        <translation>Sortie</translation>
    </message>
    <message>
        <location filename="../processing/repair_profiles_algorithm.py" line="38"/>
        <source>Repaired profiles</source>
        <translation>Profils réparés</translation>
    </message>
    <message>
        <location filename="../processing/repair_profiles_algorithm.py" line="137"/>
        <source>Repair profiles layer</source>
        <translation>Réparer une couche de profils</translation>
    </message>
    <message>
        <location filename="../processing/repair_profiles_algorithm.py" line="145"/>
        <source>Profiles</source>
        <translation>Profils</translation>
    </message>
    <message>
        <location filename="../processing/repair_profiles_algorithm.py" line="140"/>
        <source>This algorithm repair a profiles layer by adding missing topo_bat field.</source>
        <translation>Cet algorithme répare une couche de profils en ajoutant le champ topo_bat manquant.</translation>
    </message>
</context>
<context>
    <name>SaveChangesDialog</name>
    <message>
        <location filename="../ui/save_changes_dialog.ui" line="26"/>
        <source>Table data are modified</source>
        <translation>Les données ont été modifiées</translation>
    </message>
    <message>
        <location filename="../ui/save_changes_dialog.ui" line="54"/>
        <source>Do you want to save the changes?</source>
        <translation>Souhaitez-vous renregsitrer les modifications ?</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../widgets/settings_dialog.py" line="26"/>
        <source>Not set</source>
        <translation>Non définie</translation>
    </message>
    <message>
        <location filename="../ui/settings_dialog.ui" line="14"/>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <location filename="../ui/settings_dialog.ui" line="48"/>
        <source>Default elevation value when there is no Z value to extract.</source>
        <translation>Altitude par défaut lorsqu&apos;il n&apos;y a pas de valeur de Z à extraire.</translation>
    </message>
    <message>
        <location filename="../ui/settings_dialog.ui" line="51"/>
        <source>Default elevation</source>
        <translation>Altitude par défaut</translation>
    </message>
</context>
</TS>
